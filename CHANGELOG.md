Version 1.0.3, 2023-04-07
-------------------------

* Add support for RFC 7798 (HEVC/H.265) video stream

Version 1.0.2, 2020-09-13
-------------------------

* Support 64 bit unsigned values in SDP Origin.

Version 1.0.1, 2020-06-13
-------------------------

* Add exception handling between RTP session and observers.
* Start using C# 6.0 syntax. (Compiler included in .Net 4.6, .Net Core 1.0, Visual Studio 2015.)
* Fix bugs: avoid NullReferenceException, inverted condition.

Version 1.0.0, 2019-01-26
-------------------------

* Implement Digest authentication
* Enable SSL/TLS

Version 0.5.0, 2018-11-18
-------------------------

* Implement multiple RTP session observers

Version 0.4.0, 2017-12-25
-------------------------

* Notify stream consumers when streaming has stopped.
* Strengthen exception handling.
* Move stream and RTSP session code to specialised classes.

Version 0.3.0, 2017-02-19
-------------------------

* Make harcoded values configurable in runtime/application domain.
* Reduce heap allocations in the media code path.

Version 0.2.0, 2017-01-15
-------------------------

* Add partial support for RFC 3640: MPEG4 High Bit-rate AAC
* Add support for RFC 3016 (MPEG-4) visual streams
* Implement request timeout
* Implement multiple stream setup and recording

Version 0.1.0, 2016-12-08
-------------------------

* First public release
