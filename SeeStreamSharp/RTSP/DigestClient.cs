﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Security.Cryptography;

namespace SeeStreamSharp.RTSP
{
    /// <summary>
    /// Implements RFC 2617 Digest authentication (https://tools.ietf.org/html/rfc2617).
    /// </summary>
    internal class DigestClient
    {
        private static readonly char[] _hexLowerChars = new char[] {'0', '1', '2', '3',
                                                                     '4', '5', '6', '7',
                                                                     '8', '9', 'a', 'b',
                                                                     'c', 'd', 'e', 'f'};

        private const string Nonce = "nonce";
        private const string Realm = "realm";
        private const string Response = "response";
        private const string Stale = "stale";
        private const string Uri = "uri";
        private const string Username = "username";

        private const string AuthType = "Digest";

        private static MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

        internal static DigestClient Parse(Response response)
        {
            DigestClient dc = null;

            string[] auth = response.Headers.GetValues(HeaderCollection.WwwAuthenticate);
            if (auth != null)
            {
                //  Multiple "WWW-Authenticate" header lines may be present in a response.
                //  Also, a single "WWW-Authenticate" header line may contain multiple
                //  challenges. The header line parsing implementation will merge multiple
                //  header lines to look like a single header line contain multiple challenges.
                for (int i = 0; i < auth.Length; i++)
                {
                    string item = auth[i];

                    //  First find the name and value equal sign separator.
                    int equalIndex = item.IndexOf('=');
                    if (equalIndex < 0)
                        break;
                    string param = item.Substring(0, equalIndex);

                    //  If a space is present and before the equal then this item starts
                    //  with an authentication scheme name.
                    int spaceIndex = item.IndexOf(' ');
                    if (spaceIndex > 0 && spaceIndex < equalIndex)
                    {
                        //  Only Digest is implemented.
                        bool isDigest = item.StartsWith(AuthType);
                        //  Start parsing the first Digest challenge but any additional
                        //  schemes after the first Digest ends the parsing.
                        if (isDigest && dc == null)
                            dc = new DigestClient();
                        else if (dc != null)
                            break;

                        spaceIndex++;
                        param = item.Substring(spaceIndex, equalIndex - spaceIndex);
                    }

                    if (dc != null)
                    {
                        if (param.Equals(Realm))
                            dc._realm = ExtractQuoted(item, equalIndex);
                        if (param.Equals(Nonce))
                            dc._nonce = ExtractQuoted(item, equalIndex);
                        if (param.Equals(Stale))
                        {
                            string stale = ExtractQuoted(item, equalIndex);
                            if (stale != null)
                                bool.TryParse(stale, out dc._stale);
                        }
                    }
                }

                //  If not enough challenge information was extracted then forget about it.
                if (dc._realm == null || dc._nonce == null)
                    dc = null;
            }

            return dc;
        }

        private static string ExtractQuoted(string item, int equalIndex)
        {
            int start = equalIndex + 1,             //  After the equal sign.
                count = item.Length - equalIndex - 1;

            //  '"' after the equal sign and at the end needs to be discarded.
            if (item[equalIndex + 1] == '"' && item[item.Length - 1] == '"')
            {
                start++;
                count -= 2;
            }

            return item.Substring(start, count);
        }

        private static string hashString(string input)
        {
            //  This will only support ASCII content...
            byte[] bytes = new byte[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                bytes[i] = (byte)input[i];
            }

            byte[] hash = md5.ComputeHash(bytes);

            //  Build the string representation
            int size = hash.Length;
            char[] wa = new char[2 * size];

            for (int i = 0, j = 0; i < size; i++)
            {
                //  The result IS case sensitive!
                wa[j++] = _hexLowerChars[hash[i] >> 4];
                wa[j++] = _hexLowerChars[hash[i] & 0x0F];
            }

            return new string(wa);
        }

        private string _realm;
        private string _nonce;
        private string _username;
        private string _hA1;
        internal bool _stale;
        internal Request _challenged;

        private DigestClient()
        {
        }

        internal void SetUserNameAndPassword(string username, string password)
        {
            _username = username;

            string secret = username + ":" + _realm + ":" + password;
            _hA1 = hashString(secret);
        }

        internal void CreateAuthorizationValue(Request request)
        {
            //  Remove previous header if this is a resend.
            request.Headers.Remove(HeaderCollection.Authorization);

            request.Headers.Add(HeaderCollection.Authorization,
                string.Format("{0} {1}=\"{2}\"", AuthType, Username, _username));
            request.Headers.Add(HeaderCollection.Authorization,
                string.Format("{0}=\"{1}\"", Realm, _realm));
            request.Headers.Add(HeaderCollection.Authorization,
                string.Format("{0}=\"{1}\"", Nonce, _nonce));
            string url = request.Url.GetComponents(UriComponents.HttpRequestUrl, UriFormat.UriEscaped);
            request.Headers.Add(HeaderCollection.Authorization,
                string.Format("{0}=\"{1}\"", Uri, url));
            request.Headers.Add(HeaderCollection.Authorization,
                string.Format("{0}=\"{1}\"", Response, CreateResponse(request.Request.ToString(), url)));
        }

        private string CreateResponse(string method, string url)
        {
            string hA2 = hashString(method + ":" + url);

            return hashString(_hA1 + ":" + _nonce + ":" + hA2);
        }
    }
}
