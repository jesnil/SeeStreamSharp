﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;

namespace SeeStreamSharp.RTSP
{
    /// <summary>
    /// SSL/TLS protected client connection.
    /// </summary>
    /// <remarks>
    /// SSL/TLS is not strictly in RFC 2326. However, this is a simple addition.
    /// </remarks>
    internal class ClientSslConnection : ClientTcpConnection
    {
        private readonly string _host;
        private SslStream _sslStream;

        public ClientSslConnection(string scheme, IPAddress[] addressList, int port, string host)
            : base(scheme, addressList, port)
        {
            _host = host;
        }

        protected override void DoConnect()
        {
            base.DoConnect();

            if (_s.Connected)
            {
                //  A network stream requires a blocking socket.
                _s.Blocking = true;
                //  NetworkStream needs to be constructed with ownsSocket = false because the
                //  socket will be shutdown and closed when disposing in ClientConnection.
                NetworkStream ns = new NetworkStream(_s, false);
                //  SslStream needs to be constructed with leaveInnerStreamOpen = false
                //  to get the NetworkStream to be disposed when SslStream is disposed.
                _sslStream = new SslStream(ns, false);
                _sslStream.AuthenticateAsClient(_host);

                _logger.DebugFormat("[{0}] SslProtocol={1}", _loggingId, _sslStream.SslProtocol);
                _logger.DebugFormat("[{0}] IsAuthenticated={1}", _loggingId, _sslStream.IsAuthenticated);
                _logger.DebugFormat("[{0}] IsEncrypted={1}", _loggingId, _sslStream.IsEncrypted);
                _logger.DebugFormat("[{0}] IsSigned={1}", _loggingId, _sslStream.IsSigned);

                _logger.DebugFormat("[{0}] CipherAlgorithm={1}", _loggingId, _sslStream.CipherAlgorithm);
                _logger.DebugFormat("[{0}] CipherStrength={1}", _loggingId, _sslStream.CipherStrength);
                _logger.DebugFormat("[{0}] HashAlgorithm={1}", _loggingId, _sslStream.HashAlgorithm);
                _logger.DebugFormat("[{0}] HashStrength={1}", _loggingId, _sslStream.HashStrength);
            }
        }

        protected override void DoStartReceive()
        {
            using (AsyncFlowControl fc = ExecutionContext.SuppressFlow())
            {
                ArraySegment<byte> rb = Buffer;
                IAsyncResult ar = _sslStream.BeginRead(rb.Array, rb.Offset, rb.Count, ReceptionEnd, null);
            }
        }

        private void ReceptionEnd(IAsyncResult ar)
        {
            try
            {
                int read = _sslStream.EndRead(ar);

                if (read == 0)  //  End-of-stream
                    return;

                Process(read);
            }
            catch (Exception e)
            {
                AbortAll(e);
            }
        }

        protected override void DoSend(byte[] data)
        {
            _sslStream.Write(data);
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                //  This will dispose both the SslStream object as well as the
                //  inner NetworkStream object but leave the socket open.
                _sslStream.Dispose();
            }
            finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
