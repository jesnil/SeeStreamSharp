﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System.Text;

namespace SeeStreamSharp.RTSP
{
    internal abstract class Message
    {
        public enum Requests
        {
            RESPONSE = 0,
            OPTIONS = 1,
            DESCRIBE,
            SETUP,
            PLAY,
            PAUSE,
            TEARDOWN,
            GET_PARAMETER,
            SET_PARAMETER,
            ANNOUNCE
        }

        public static readonly char CR = '\r';
        public static readonly char LF = '\n';

        private readonly HeaderCollection _headers = new HeaderCollection();

        public readonly Requests Request;

        public byte[] Entity { get; internal set; }

        public int ContentLength
        {
            get { return _headers.ContentLengthValue; }
            set { _headers.ContentLengthValue = value; }
        }

        public int CSeq
        {
            get { return _headers.CSeqValue; }
            protected set
            {
                _headers.CSeqValue = value;
            }
        }

        public HeaderCollection Headers { get { return _headers; } }

        protected Message(Requests request)
        {
            Request = request;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(FirstLineToString());
            //  End first line
            sb.Append(CR);
            sb.Append(LF);
            _headers.ToString(sb);
            //  Additional zero lenght line to signal end of message.
            sb.Append(CR);
            sb.Append(LF);

            return sb.ToString();
        }

        protected abstract string FirstLineToString();
    }
}
