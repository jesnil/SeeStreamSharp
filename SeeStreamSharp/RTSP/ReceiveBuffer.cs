﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Properties;
using System;
using System.Diagnostics;
using System.Text;

namespace SeeStreamSharp.RTSP
{
    internal sealed class ReceiveBuffer
    {
        internal const byte ASCII_DOLLAR = 0x24;
        internal const byte CR = 13;
        internal const byte LF = 10;

        private readonly ILog _logger = LogManager.GetLogger<ReceiveBuffer>();
        private readonly int _loggingId;

        private readonly Encoding _encoder = Encoding.UTF8;

        private readonly IMessageReceiver _receiver;
        private byte[] _receiveBuffer;
        private int _receiveIndex;

        private Message _currentMessage;
        private bool _messageCompleted;

        public ArraySegment<byte> Buffer
        {
            get
            {
                Debug.Assert(_receiveBuffer.Length - _receiveIndex > 0, "Buffer underflow");
                return new ArraySegment<byte>(_receiveBuffer, _receiveIndex, _receiveBuffer.Length - _receiveIndex);
            }
        }

        internal ReceiveBuffer(IMessageReceiver receiver)
        {
            _receiver = receiver;

            _receiveBuffer = new byte[Settings.Default.RtspReceiveBufferSize];
            _receiveIndex = 0;

            //  When logging is disabled it should not cause any garbage (heap
            //  allocations) at all in the key code path.
            AddBytesLog1Action = new Action<FormatMessageHandler>(AddBytesLog1);
            AddBytesLog2Action = new Action<FormatMessageHandler>(AddBytesLog2);
            AddBytesLog3Action = new Action<FormatMessageHandler>(AddBytesLog3);
            AddBytesLog4Action = new Action<FormatMessageHandler>(AddBytesLog4);
            AddBytesLog5Action = new Action<FormatMessageHandler>(AddBytesLog5);
            AddBytesLog6Action = new Action<FormatMessageHandler>(AddBytesLog6);
            ParseInterleavedDataLogAction = new Action<FormatMessageHandler>(ParseInterleavedDataLog);

            _loggingId = GetHashCode();
            _logger.DebugFormat("[{0}] created", _loggingId);
        }

        #region No heap allocation logging

        private readonly Action<FormatMessageHandler> AddBytesLog1Action;

        private void AddBytesLog1(FormatMessageHandler fmh)
        {
            fmh("[{0}] AddBytes() enterd", _loggingId);
        }

        private readonly Action<FormatMessageHandler> AddBytesLog2Action;

        private void AddBytesLog2(FormatMessageHandler fmh)
        {
            fmh("[{0}] AddBytes() buffer totaly consumed", _loggingId);
        }

        private readonly Action<FormatMessageHandler> AddBytesLog3Action;

        private void AddBytesLog3(FormatMessageHandler fmh)
        {
            fmh("[{0}] AddBytes() buffer remaining {1}", _loggingId, _receiveIndex);
        }

        private int _newSize;
        private readonly Action<FormatMessageHandler> AddBytesLog4Action;

        private void AddBytesLog4(FormatMessageHandler fmh)
        {
            fmh("[{0}] AddBytes() buffer size calculated to {1}", _loggingId, _newSize);
        }

        private readonly Action<FormatMessageHandler> AddBytesLog5Action;

        private void AddBytesLog5(FormatMessageHandler fmh)
        {
            fmh("[{0}] AddBytes() buffer size estimated to {1}", _loggingId, _newSize);
        }

        private readonly Action<FormatMessageHandler> AddBytesLog6Action;

        private void AddBytesLog6(FormatMessageHandler fmh)
        {
            fmh("[{0}] AddBytes() exiting", _loggingId);
        }

        private readonly Action<FormatMessageHandler> ParseInterleavedDataLogAction;

        private int _index, _count;

        private void ParseInterleavedDataLog(FormatMessageHandler fmh)
        {
            fmh("[{0}] ParseInterleavedData() extracted channel {1} size {2}",
                    _loggingId, _receiveBuffer[_index + 1], _count);
        }

        #endregion

        public void AddBytes(int count)
        {
            _logger.Trace(AddBytesLog1Action);

            int indexAfterLastByte = _receiveIndex + count,
                needToRead,
                index = 0;

            /*
                When entering the loop these states are possible:

                - The buffer was previously empty.

                - Start of interleaved data is already at the head of _receiveBuffer. More data
                  became available.
                  The complete interleaved packet needs to be assembled in _receiveBuffer before
                  being able to forward it up the protocol stack.

                - Is in the process of parsing an incomplete RTSP message. More data became
                  available.
                  The buffer is parsed using a line-by-line algorithm. When determening that more
                  data is required the start of a partial line or the start of the message entity
                  is located at the head of _receiveBuffer.

                In each iteration of the loop RTSP messages and interleaved data may be randomly
                mixed.
             */
            do
            {
                //  When entering the loop (index == 0), or on the next iteration (index > 0),
                //  and not in the process of parsing a RTSP message (_currentMessage == null),
                //  check if _receiveBuffer[index] == ASCII_DOLLAR.
                if (_currentMessage == null)
                {
                    needToRead = ParseInterleavedData(ref index, indexAfterLastByte);
                    if (needToRead != 0)
                        break;
                }
                else
                {
                    //  Is in the process of parsing a RTSP message. Check if it was completed
                    //  but without its trailing entity. If so try to process it now.
                    needToRead = TryProcessMessage(ref index, indexAfterLastByte);
                    if (needToRead != 0)
                        break;
                }

                //  At the start of a new message or additional header lines of a previously
                //  incompleted message.
                needToRead = ParseMessageLines(ref index, indexAfterLastByte);
                if (needToRead != 0)
                    break;

                //  Try processing a completed message before exiting the loop. Will enable
                //  processing of interleaved data in the next iteration of the loop.
                needToRead = TryProcessMessage(ref index, indexAfterLastByte);
                if (needToRead != 0)
                    break;

                Debug.Assert(index <= indexAfterLastByte, "Buffer overrun in loop", "offset = {0}, offsetAfterLastByte = {1}",
                    index, indexAfterLastByte);
            } while (index < indexAfterLastByte);

            if (index == indexAfterLastByte)
            {
                _logger.Trace(AddBytesLog2Action);

                _receiveIndex = 0;
            }
            else
            {
                Debug.Assert(index <= indexAfterLastByte, "Buffer overrun after loop", "offset = {0}, offsetAfterLastByte = {1}",
                    index, indexAfterLastByte);

                _receiveIndex = indexAfterLastByte - index;
                Array.Copy(_receiveBuffer, index, _receiveBuffer, 0, _receiveIndex);

                _logger.Trace(AddBytesLog3Action);
            }

            //  First check that the buffer has room for a known amount of bytes.
            if (needToRead > 0 && _receiveBuffer.Length - _receiveIndex < needToRead)
            {
                _newSize = _receiveIndex + needToRead + 32;

                _logger.Trace(AddBytesLog4Action);

                byte[] temp = new byte[_newSize];
                Array.Copy(_receiveBuffer, temp, _receiveIndex);
                _receiveBuffer = temp;
            }
            //  If an unknown amount is required and there is less than 512 bytes available
            //  increase the total buffer size by 512.
            //  This assumes that the added buffer space is filled up by each additional read
            //  but still not able to "consume" it withhout even more data. (_receiveIndex
            //  grows by each read.)
            if (needToRead < 0 && _receiveBuffer.Length - _receiveIndex < 512)
            {
                _newSize = _receiveBuffer.Length + 512;

                _logger.Trace(AddBytesLog5Action);

                byte[] temp = new byte[_newSize];
                Array.Copy(_receiveBuffer, temp, _receiveIndex);
                _receiveBuffer = temp;
            }

            Debug.Assert(needToRead <= _receiveBuffer.Length - _receiveIndex, "Buffer to small", "neadToRead = {0}", needToRead);

            _logger.Trace(AddBytesLog6Action);
        }

        /// <summary>
        /// Adds possible entity to a completed RTSP message before forwarding it up the protocol stack.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="indexAfterLastByte"></param>
        /// <returns>Non-zero value signals that more data needs to be received.</returns>
        private int TryProcessMessage(ref int index, int indexAfterLastByte)
        {
            _logger.TraceFormat("[{0}] TryProcessMessage() enterd", _loggingId);

            if (_messageCompleted)
            {
                int cl = _currentMessage.ContentLength;
                if (cl > 0)
                {
                    int needToRead = cl - (indexAfterLastByte - index);
                    if (needToRead > 0)
                    {
                        _logger.DebugFormat("[{0}] TryProcessMessage() exiting - need to read {1}",
                            _loggingId, needToRead);
                        return needToRead;
                    }

                    _logger.DebugFormat("[{0}] TryProcessMessage() adding entity, lenght={1}",
                        _loggingId, cl);

                    byte[] entity = new byte[cl];
                    Array.Copy(_receiveBuffer, index, entity, 0, cl);
                    index += cl;

                    _currentMessage.Entity = entity;
                }

                _receiver.Process(_currentMessage);

                _messageCompleted = false;
                _currentMessage = null;
            }

            _logger.TraceFormat("[{0}] TryProcessMessage() exiting - no need to read", _loggingId);

            return 0;
        }

        /// <summary>
        /// Checks if the first byte of the buffer is an interleaved start token. Processes each packet in a loop.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="indexAfterLastByte"></param>
        /// <returns>Non-zero value signals that more data needs to be received.</returns>
        private int ParseInterleavedData(ref int index, int indexAfterLastByte)
        {
            while (index < indexAfterLastByte && _receiveBuffer[index] == ASCII_DOLLAR)
            {
                //  Make sure the full interleaved header is available.
                int headerAvailable = indexAfterLastByte - index;
                if (headerAvailable < 4)
                    return 4 - headerAvailable;

                int count = (_receiveBuffer[index + 2] << 8) | _receiveBuffer[index + 3];

                //  If not all of the payload is in the buffer this has to be re-parsed.
                int neadToRead = count - (indexAfterLastByte - index - 4);
                if (neadToRead > 0)
                    return neadToRead;

                _index = index;
                _count = count;
                _logger.Trace(ParseInterleavedDataLogAction);

                _receiver.Process(_receiveBuffer[index + 1], new ArraySegment<byte>(_receiveBuffer, index + 4, count));

                index += count + 4;
            }

            return 0;
        }

        /// <summary>
        /// Parse buffer into lines of a RTSP message.
        /// </summary>
        /// <remarks>
        /// A line is defined as a sequence of characters followed by a carriage
        /// return ('\r', 13) immediately followed by a line feed ('\n', 10).
        /// <para>
        /// Successfull end is a zero lenght line. (Two consecutive pairs of "\r\n".)
        /// </para>
        /// </remarks>
        /// <param name="index"></param>
        /// <param name="indexAfterLastByte"></param>
        /// <returns>Non-zero value signals that more data needs to be received.</returns>
        private int ParseMessageLines(ref int index, int indexAfterLastByte)
        {
            int sol = index;

            for (int eol = index; eol < indexAfterLastByte; eol++)
            {
#if DEBUG
                if (_receiveBuffer[eol] > 127)
                    throw new InvalidOperationException();
                if (_receiveBuffer[eol] < 32)
                {
                    if (_receiveBuffer[eol] == 0xa || _receiveBuffer[eol] == 0xd)
                    {
                    }
                    else
                        throw new InvalidOperationException();
                }
#endif

                if (_receiveBuffer[eol] == CR)
                {
                    eol++;
                    if (eol >= indexAfterLastByte)
                        return -1;  //  Need more data.

                    if (_receiveBuffer[eol] != LF)
                        throw new RtspException("Message parsing encountered CR not followed by LF");

                    //  Empty line marks the end of the message headers. Return
                    //  to process the message entity or interleaved data.
                    if (eol - sol == 1)
                    {
                        _logger.DebugFormat("[{0}] ParseMessageLines() extracted empty line", _loggingId);

                        if (_currentMessage == null)
                            throw new RtspException("Unexpected end-of-message marker");

                        index = eol + 1;    //  Mark this empty line as "consumed".
                        _messageCompleted = true;
                        return 0;           //  Message completed and now "idle".
                    }

                    string line = _encoder.GetString(_receiveBuffer, sol, eol - sol - 1);

                    _logger.DebugFormat("[{0}] ParseMessageLines() extracted line '{1}'",
                        _loggingId, line);

                    //  Set start of next line to the byte after this LF.
                    //  But don't increment eol! It will be incremented by the for loop.
                    sol = eol + 1;
                    index = sol;    //  "Consume" this line.

                    //  This is the first line of a status or request message.
                    if (_currentMessage == null)
                    {
                        Message msg = Response.ParseResponseLine(line);
                        if (msg == null)
                            msg = Request.ParseRequestLine(line);

                        if (msg == null)
                        {
                            _logger.ErrorFormat("[{0}] ParseMessageLines() not a message line '{1}'",
                                _loggingId, line);
                            throw new RtspException("Unable to parse first line of message");
                        }
                        _currentMessage = msg;
                    }
                    else
                        _currentMessage.Headers.ParseLine(line);
                }
            }

            return -1;
        }
    }
}
