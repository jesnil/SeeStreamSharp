﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SeeStreamSharp.RTSP
{
    internal static class ClientConnectionFactory
    {
        private static readonly List<ClientConnection> _activeConnections = new List<ClientConnection>();
        private static IClientConnection _precreated;

        /// <summary>
        /// Setting a precreated <see cref="IClientConnection"/> enables unit testing.
        /// </summary>
        /// <param name="connection"></param>
        internal static void SetPrecreatedConnection(IClientConnection connection)
        {
            _precreated = connection;
        }

        public static void CheckIfImplemented(string scheme)
        {
            bool schemeValid = scheme.Equals(Presentation.UriSchemeRtsp, StringComparison.OrdinalIgnoreCase) ||
                             //scheme.Equals(Presentation.UriSchemeRtspu, StringComparison.OrdinalIgnoreCase) ||
                             scheme.Equals(Presentation.UriSchemeRtsps, StringComparison.OrdinalIgnoreCase);

            if (!schemeValid)
                throw new ArgumentException("Unsupported scheme");
        }

        public static Task<IClientConnection> GetConnectionAsync(Uri url)
        {
            if (_precreated != null)
            {
                TaskCompletionSource<IClientConnection> tcs = new TaskCompletionSource<IClientConnection>();
                tcs.SetResult(_precreated);
                return tcs.Task;
            }

            Task<IPAddress[]> resolv;

            //  Explicit IP address parsing is required as it is not handled by BeginGetHostEntry().
            IPAddress address;
            if (IPAddress.TryParse(url.DnsSafeHost, out address))
            {
                TaskCompletionSource<IPAddress[]> tcs = new TaskCompletionSource<IPAddress[]>(url);
                tcs.SetResult(new IPAddress[] { address });
                resolv = tcs.Task;
            }
            else
            {
                using (AsyncFlowControl acf = ExecutionContext.SuppressFlow())
                {
                    resolv = Task<IPAddress[]>.Factory.FromAsync(Dns.BeginGetHostEntry,
                                                                 ResolvTaskEndGetHostEntry,
                                                                 url.DnsSafeHost,
                                                                 url);
                }
            }

            return resolv.ContinueWith<IClientConnection>(ResolvedContinuation);
        }

        private static IPAddress[] ResolvTaskEndGetHostEntry(IAsyncResult ar)
        {
            //  Any exception looking up the host name will be thrown by the
            //  call to Dns.EndGetHostEntry().
            IPHostEntry he = Dns.EndGetHostEntry(ar);
            return he.AddressList;
        }

        private static IClientConnection ResolvedContinuation(Task<IPAddress[]> resolved)
        {
            //  This will trigger an exception if resolved task is faulted.
            IPAddress[] addressList = resolved.Result;
            Uri url = resolved.AsyncState as Uri;

            string scheme = url.Scheme;
            int port = url.Port;
            ClientConnection con = null;

            lock (_activeConnections)
            {
                foreach (ClientConnection connection in _activeConnections)
                {
                    if (connection.IsCompatibleConnection(scheme, addressList, port))
                        break;
                }

                if (con == null)
                {
                    if (scheme.Equals(Presentation.UriSchemeRtspu, StringComparison.OrdinalIgnoreCase))
                        con = new ClientUdpConnection(Presentation.UriSchemeRtspu, addressList, port);
                    else
                    {
                        if (scheme.Equals(Presentation.UriSchemeRtsps, StringComparison.OrdinalIgnoreCase))
                            con = new ClientSslConnection(Presentation.UriSchemeRtsps, addressList, port, url.Host);
                        else
                            con = new ClientTcpConnection(Presentation.UriSchemeRtsp, addressList, port);
                    }

                    //_activeConnections.Add(con);

                    con.Connect();
                }
            }

            return con;
        }

        /*
            This should be combined with the above GetConnectionAsync(Uri url) method.

        public static Task<Connection> GetAsync(Uri url)
        {
            TaskCompletionSource<Connection> tcs;

            foreach (Connection conn in _activeConnections)
            {
                if (conn.Connected)
                {
                    int eq = Uri.Compare(url, conn.Url, UriComponents.SchemeAndServer, UriFormat.Unescaped, StringComparison.OrdinalIgnoreCase);
                    if (eq == 0)
                    {
                        tcs = new TaskCompletionSource<Connection>();
                        tcs.SetResult(conn);
                        return tcs.Task;
                    }
                }
                else
                {
                    _activeConnections.Remove(conn);
                }
            }

            tcs = new TaskCompletionSource<Connection>();
            //tcs.SetResult(null);
            return tcs.Task;
        }
        */
    }
}
