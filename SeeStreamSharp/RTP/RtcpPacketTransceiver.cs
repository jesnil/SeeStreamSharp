﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;

namespace SeeStreamSharp.RTP
{
    internal sealed class RtcpPacketTransceiver : IObserver<ArraySegment<byte>>
    {
        private readonly IObserver<RtcpFixedHeader> _rtcpPacketObserver;

        internal RtcpPacketTransceiver(IObserver<RtcpFixedHeader> rtcpPacketObserver)
        {
            _rtcpPacketObserver = rtcpPacketObserver;
        }

        #region IObserver<ArraySegment<byte>>

        public void OnNext(ArraySegment<byte> value)
        {
            RtcpFixedHeader header;
            int consumed = RtcpFixedHeader.FromBytes(value, out header);
            ArraySegment<byte> payload = new ArraySegment<byte>(value.Array, value.Offset + consumed, value.Count - consumed);

            _rtcpPacketObserver.OnNext(header);
        }

        public void OnError(Exception error)
        {
            _rtcpPacketObserver.OnError(error);
        }

        public void OnCompleted()
        {
            _rtcpPacketObserver.OnCompleted();
        }

        #endregion
    }
}
