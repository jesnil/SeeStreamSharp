﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;

namespace SeeStreamSharp.RTP
{
    /// <summary>
    /// Provides a RTP session packet transceiver.
    /// </summary>
    /// <remarks>
    /// Implements packet reception (and future transmission) session mamagement as described
    /// in RFC 3550 A Transport Protocol for Real-Time Applications
    /// (https://tools.ietf.org/html/rfc3550).
    /// </remarks>
    public sealed class Session : IObservable<Packet>
    {
        private const int RTP_SEQ_MOD = 1 << 16;
        private const int MAX_DROPOUT = 3000;
        private const int MAX_MISORDER = 100;

        private readonly SessionSubject<Packet> _rtpObservers;
        private readonly SessionSubject<RtcpFixedHeader> _rtcpObservers;

        private int highestSequence;
        private int cycles;
        private int baseSequence;
        private int badSequence;

        internal SessionSubject<Packet> RtpObservers { get { return _rtpObservers; } }

        internal SessionSubject<RtcpFixedHeader> RtcpObservers { get { return _rtcpObservers; } }

        internal Session()
        {
            _rtpObservers = new SessionSubject<Packet>(UpdateSequence);
            _rtcpObservers = new SessionSubject<RtcpFixedHeader>(null);
        }

        private void UpdateSequence(ref Packet packet)
        {
            if (baseSequence == 0)
            {
                baseSequence = packet.Header.Sequence;
                highestSequence = (ushort)baseSequence;

                //  An "invalid" start value makes an initial "seq == bad_seq" test to always be "false".
                badSequence = RTP_SEQ_MOD;
            }

            //  An unsigned 16 bit result is automatically an absolut delta value.
            ushort delta = (ushort)(packet.Header.Sequence - highestSequence);
            if (delta < MAX_DROPOUT)
            {
                //  In order, with permissible gap.
                if (packet.Header.Sequence < highestSequence)
                {
                    //  Sequence number wrapped - count another 64K cycle.
                    cycles += RTP_SEQ_MOD;
                }
                highestSequence = packet.Header.Sequence;
            }
            else if (delta <= RTP_SEQ_MOD - MAX_MISORDER)
            {
                //  The sequence number made a very large jump.
                //  The first time getting here will result in a false condition. Two sequential
                //  packets -- assume that the other side restarted without telling us so just
                //  re -sync (i.e., pretend this was the first packet).
                if (packet.Header.Sequence == badSequence)
                {
                    baseSequence = packet.Header.Sequence;
                    highestSequence = baseSequence;
                    badSequence = RTP_SEQ_MOD;
                    cycles = 0;
                }
                else
                    badSequence = packet.Header.Sequence + 1;
            }

            if (cycles != 0)
                packet.Header.Sequence = packet.Header.Sequence | cycles;
        }

        #region IObservable<Packet>

        /// <summary>
        /// Subscribes an observer to the media packet stream of this RTP Session.
        /// </summary>
        /// <param name="observer">Observer to subscribe to the packet stream.</param>
        /// <returns>Media packet stream object that is unsubscribed by calling dispose.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="observer"/> is null.</exception>
        public IDisposable Subscribe(IObserver<Packet> observer)
        {
            if (observer == null)
                throw new ArgumentNullException("observer");

            return _rtpObservers.Subscribe(observer);
        }

        #endregion
    }
}
