﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Threading;

namespace SeeStreamSharp.RTP
{
    /// <summary>
    /// Enables multiple observers on a specific packet type in a RTP session.
    /// </summary>
    /// <typeparam name="T"><see cref="Packet"/> or <see cref="RtcpFixedHeader"/></typeparam>
    internal sealed class SessionSubject<T> : IObserver<T>
    {
        internal delegate void Processor(ref T instance);

        private readonly Processor _processor;
        private volatile object _observers;
        private Exception _error;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="processor">Delegate to enable parent object to process a stream item
        /// before forwarding it to all current observers.</param>
        internal SessionSubject(Processor processor)
        {
            _processor = processor;
        }

        internal IDisposable Subscribe(IObserver<T> observer)
        {
            object current;
            object replacement = null;
            ObserverDisposable addedObserver = new ObserverDisposable(this, observer);

            do
            {
                current = _observers;

                //  First check if session is already closed.
                if (current == NopDisposable.Instance)
                {
                    if (_error != null)
                        addedObserver.DoError(_error);
                    else
                        addedObserver.DoCompleted();

                    return NopDisposable.Instance;
                }

                //  If no observer yet, set as the first observer.
                if (current == null)
                    replacement = addedObserver;
                else
                {
                    //  If already multiple observers, increment the array.
                    ObserverDisposable[] currentArray = current as ObserverDisposable[];
                    if (currentArray != null)
                    {
                        ObserverDisposable[] newArray = new ObserverDisposable[currentArray.Length + 1];

                        Array.Copy(currentArray, newArray, currentArray.Length);
                        newArray[currentArray.Length] = addedObserver;
                        replacement = newArray;
                    }
                    else
                    {
                        //  If session not ended, no single observer, and not already an array
                        //  of observers, then create an array of the first observer and the
                        //  observer to be added in the current call.
                        ObserverDisposable firstObserver = current as ObserverDisposable;
                        replacement = new ObserverDisposable[] { firstObserver, addedObserver };
                    }
                }
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);

            return addedObserver;
        }

        private void Unsubscribe(ObserverDisposable observer)
        {
            object current;
            object replacement = null;

            do
            {
                current = _observers;

                //  If session already closed then no need to remove individual observer.
                if (current == NopDisposable.Instance)
                    return;

                ObserverDisposable[] currentArray = current as ObserverDisposable[];
                if (currentArray != null)
                {
                    int i = currentArray.Length - 1;
                    do
                    {
                        if (currentArray[i] == observer)
                            break;
                        i--;
                    } while (i >= 0);

                    //  Argument is not in the list, bail out.
                    if (i < 0)
                        return;

                    int length = currentArray.Length;
                    //  If array length is 1 and the item in the array is the only one removed,
                    //  remove the list completely.
                    if (length == 1)
                        //  Set it back to intial value null indicating no observers.
                        replacement = null;
                    else
                    {
                        ObserverDisposable[] newArray = new ObserverDisposable[length - 1];
                        Array.Copy(currentArray, 0, newArray, 0, i);
                        Array.Copy(currentArray, i + 1, newArray, i, length - i - 1);
                        replacement = newArray;
                    }
                }
                else
                {
                    //  If the current single observer is not the one to remove then do nothing.
                    //  It could be that current == NopDisposable or current == null. In both cases
                    //  there is also no need to replace current.
                    if (current != observer)
                        return;
                    //  Set it back to intial value null indicating no observers.
                    replacement = null;
                }
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);
        }

        void IObserver<T>.OnNext(T value)
        {
            //  Parent object is first to process the stream item before forwarding it to
            //  all observers.
            _processor?.Invoke(ref value);

            ObserverDisposable observer = _observers as ObserverDisposable;
            if (observer != null)
            {
                observer.DoNext(value);
                return;
            }
            ObserverDisposable[] observers = _observers as ObserverDisposable[];
            if (observers != null)
            {
                for (int i = 0; i < observers.Length; i++)
                {
                    if (observers[i] != null)
                        observers[i].DoNext(value);
                }
            }
        }

        void IObserver<T>.OnError(Exception error)
        {
            if (error == null)
                throw new ArgumentNullException("error");

            //  If not yet set then assign the error.
            if (Interlocked.CompareExchange(ref _error, error, null) != null)
                return;

            object current;
            object replacement = NopDisposable.Instance;

            do
            {
                current = _observers;
                if (current == NopDisposable.Instance)
                    break;
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);

            ObserverDisposable observer = current as ObserverDisposable;
            if (observer != null)
            {
                observer.DoError(error);
                return;
            }
            ObserverDisposable[] observers = current as ObserverDisposable[];
            if (observers != null)
            {
                for (int i = 0; i < observers.Length; i++)
                {
                    if (observers[i] != null)
                        observers[i].DoError(error);
                }
            }
        }

        void IObserver<T>.OnCompleted()
        {
            object current;
            object replacement = NopDisposable.Instance;

            do
            {
                current = _observers;
                if (current == NopDisposable.Instance)
                    break;
            } while (Interlocked.CompareExchange(ref _observers, replacement, current) != current);

            ObserverDisposable observer = current as ObserverDisposable;
            if (observer != null)
            {
                observer.DoCompleted();
                return;
            }
            ObserverDisposable[] observers = current as ObserverDisposable[];
            if (observers != null)
            {
                for (int i = 0; i < observers.Length; i++)
                {
                    if (observers[i] != null)
                        observers[i].DoCompleted();
                }
            }
        }

        /// <summary>
        /// Object returned as the <see cref="IDisposable"/> used to enable the observer to
        /// end the observation.
        /// </summary>
        /// <remarks>
        /// This is also used to unsubscribe the observer when an exception occurs in the observer.
        /// </remarks>
        private class ObserverDisposable : IDisposable
        {
            private SessionSubject<T> _subject;
            private IObserver<T> _observer;

            internal ObserverDisposable(SessionSubject<T> subject, IObserver<T> observer)
            {
                _subject = subject;
                _observer = observer;
            }

            public void Dispose()
            {
                //  Make sure that Unsubscribe() is only called one time.
                IObserver<T> observer = Interlocked.Exchange(ref _observer, null);
                if (observer == null)
                    return;

                _subject.Unsubscribe(this);
                _subject = null;
            }

            internal void DoNext(T value)
            {
                try
                {
                    _observer.OnNext(value);
                }
                catch
                {
                    //  Remove this observer because it has failed and should no longer be called.
                    Dispose();
                }
            }

            internal void DoCompleted()
            {
                try
                {
                    _observer.OnCompleted();
                }
                catch
                {
                    //  No need to Dispose() as the list of observers have already been replaced.
                }
            }

            internal void DoError(Exception exception)
            {
                try
                {
                    _observer.OnError(exception);
                }
                catch
                {
                    //  No need to Dispose() as the list of observers have already been replaced.
                }
            }
        }

        /// <summary>
        /// A singleton of this class is used as a marker for a session that is closed.
        /// </summary>
        private class NopDisposable : IDisposable
        {
            public static IDisposable Instance = new NopDisposable();

            private NopDisposable()
            {
            }

            public void Dispose()
            {
                //  Does nothing.
            }
        }
    }

}
