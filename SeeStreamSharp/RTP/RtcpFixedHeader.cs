﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections.Specialized;
using System.Net;

namespace SeeStreamSharp.RTP
{
    /// <summary>
    /// Represents the header of the RTCP packet.
    /// </summary>
    /// <remarks>
    /// This is intentionally a struct as it is intended to be passed as an argument in method
    /// calls. A struct avoids heap allocations.
    /// </remarks>
    internal struct RtcpFixedHeader
    {
        public const int FixedHeaderSize = 8;

        /*
         *  "The static field variable initializers of a class correspond to a sequence of
         *  assignments that are executed in the textual order in which they appear in the
         *  class declaration."
         */
        private static readonly BitVector32.Section s_payloadType = BitVector32.CreateSection(255);
        private static readonly BitVector32.Section s_rrCount = BitVector32.CreateSection(31, s_payloadType);
        private static readonly BitVector32.Section s_padding = BitVector32.CreateSection(1, s_rrCount);
        private static readonly BitVector32.Section s_version = BitVector32.CreateSection(3, s_padding);

        private readonly BitVector32 _first16bit;

        public int Length { get; private set; }

        public int Version
        {
            get { return _first16bit[s_version]; }
        }

        public bool Padding
        {
            get { return _first16bit[s_padding] == 1; }
        }

        public int CsrcCount
        {
            get { return _first16bit[s_rrCount]; }
        }

        public int PayloadType
        {
            get { return _first16bit[s_payloadType]; }
        }

        public int Ssrc { get; private set; }

        internal RtcpFixedHeader(BitVector32 flags, int length, int ssrc)
            : this()
        {
            if (flags[s_version] != 2)
                throw new ArgumentOutOfRangeException("flags", "Unsupported packet version: " +
                                                      flags[s_version]);
            if (length > ushort.MaxValue)
                throw new ArgumentOutOfRangeException("length", "Invalid packet length: " +
                                                      flags[s_version]);

            _first16bit = flags;
            Length = length;
            Ssrc = ssrc;
        }

        internal static int FromBytes(ArraySegment<byte> input, out RtcpFixedHeader header)
        {
            if (input.Count < FixedHeaderSize)
                throw new ArgumentOutOfRangeException("input", "Fixed header size underflow");

            int net32 = BitConverter.ToInt32(input.Array, input.Offset);
            int host32 = IPAddress.NetworkToHostOrder(net32);

            int length = (ushort)host32;
            BitVector32 flags = new BitVector32(host32 >> 16);

            net32 = BitConverter.ToInt32(input.Array, input.Offset + 4);
            int ssrc = IPAddress.NetworkToHostOrder(net32);

            header = new RtcpFixedHeader(flags, length, ssrc);

            return FixedHeaderSize;
        }
    }
}
