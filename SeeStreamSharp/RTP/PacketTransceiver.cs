﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Net;

namespace SeeStreamSharp.RTP
{
    internal sealed class PacketTransceiver : IObserver<ArraySegment<byte>>
    {
        private readonly IObserver<Packet> _rtpPacketObserver;

        internal PacketTransceiver(IObserver<Packet> rtpPacketObserver)
        {
            _rtpPacketObserver = rtpPacketObserver;
        }

        #region IObserver<ArraySegment<byte>>

        public void OnNext(ArraySegment<byte> value)
        {
            RtpFixedHeader header;
            int offset = RtpFixedHeader.FromBytes(value, out header);

            //  Calculate actual start of payload.
            ArraySegment<byte> csrc;
            if (header.CsrcCount > 0)
            {
                csrc = new ArraySegment<byte>(value.Array, value.Offset + offset, header.CsrcCount * 4);
                offset += csrc.Count;
                if (value.Count - value.Offset < offset)
                    throw new ArgumentOutOfRangeException("data", "Contributed header size underflow");
            }
            else
                csrc = new ArraySegment<byte>();

            ArraySegment<byte> extendedHeader;
            if (header.Extension)
            {
                int net32 = BitConverter.ToInt32(value.Array, value.Offset + offset);
                int host32 = IPAddress.NetworkToHostOrder(net32);

                extendedHeader = new ArraySegment<byte>(value.Array,
                    value.Offset + offset, (1 + (ushort)host32) * 4);

                offset += extendedHeader.Count;
                if (value.Count - value.Offset < offset)
                    throw new ArgumentOutOfRangeException("data", "Extended header size underflow");
            }
            else
            {
                extendedHeader = new ArraySegment<byte>();
            }

            ArraySegment<byte> payload = new ArraySegment<byte>(value.Array, value.Offset + offset,
                                                                value.Count - offset);

            _rtpPacketObserver.OnNext(new Packet(header, csrc, extendedHeader, payload));
        }

        public void OnError(Exception error)
        {
            _rtpPacketObserver.OnError(error);
        }

        public void OnCompleted()
        {
            _rtpPacketObserver.OnCompleted();
        }

        #endregion
    }
}
