﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System;
using System.Collections.Specialized;
using System.Net;

namespace SeeStreamSharp.RTP
{
    /// <summary>
    /// Represents the header of the RTP packet.
    /// </summary>
    /// <remarks>
    /// This is intentionally a struct as it is intended to be passed as an argument in method
    /// calls. A struct avoids heap allocations.
    /// </remarks>
    public struct RtpFixedHeader
    {
        public const int FixedHeaderSize = 12;

        /*
         *  "The static field variable initializers of a class correspond to a sequence of
         *  assignments that are executed in the textual order in which they appear in the
         *  class declaration."
         */
        private static readonly BitVector32.Section s_payloadType = BitVector32.CreateSection(127);
        private static readonly BitVector32.Section s_marker = BitVector32.CreateSection(1, s_payloadType);
        private static readonly BitVector32.Section s_CsrcCount = BitVector32.CreateSection(15, s_marker);
        private static readonly BitVector32.Section s_extension = BitVector32.CreateSection(1, s_CsrcCount);
        private static readonly BitVector32.Section s_padding = BitVector32.CreateSection(1, s_extension);
        private static readonly BitVector32.Section s_version = BitVector32.CreateSection(3, s_padding);

        private BitVector32 _first16bit;

        /// <summary>
        /// Gets the packet version number.
        /// </summary>
        /// <value>
        /// This implementation only supports version 2.
        /// </value>
        public int Version
        {
            get { return _first16bit[s_version]; }
        }

        /// <summary>
        /// Get the packet padding flag.
        /// </summary>
        /// <value>
        /// Value is True if bit is set.
        /// <para>
        /// The padding bit can be reset using the <see cref="RtpFixedHeader.RemovePadding(ref ArraySegment{byte})"/>.
        /// </para>
        /// </value>
        public bool Padding
        {
            get { return _first16bit[s_padding] == 1; }
            private set { _first16bit[s_padding] = value ? 1 : 0; }
        }

        /// <summary>
        /// Gets the packet extension flag.
        /// </summary>
        public bool Extension
        {
            get { return _first16bit[s_extension] == 1; }
        }

        /// <summary>
        /// Gets the packet CSRC count.
        /// </summary>
        public int CsrcCount
        {
            get { return _first16bit[s_CsrcCount]; }
        }

        /// <summary>
        /// Gets the packet marker flag.
        /// </summary>
        public bool Marker
        {
            get { return _first16bit[s_marker] == 1; }
        }

        /// <summary>
        /// Gets the packet payload type.
        /// </summary>
        public int PayloadType
        {
            get { return _first16bit[s_payloadType]; }
        }

        /// <summary>
        /// Gets the extended sequence number.
        /// </summary>
        public int Sequence { get; internal set; }

        /// <summary>
        /// Gets the packet timestamp.
        /// </summary>
        public int TimeStamp { get; private set; }

        /// <summary>
        /// Gets the packet synchronization source identifier.
        /// </summary>
        public int Ssrc { get; private set; }

        internal RtpFixedHeader(BitVector32 flags, int sequence, int timeStamp, int ssrc)
            : this()
        {
            if (flags[s_version] != 2)
            {
                throw new ArgumentOutOfRangeException("flags", "Unsupported packet version: " +
                                                      flags[s_version]);
            }

            _first16bit = flags;
            Sequence = sequence;
            TimeStamp = timeStamp;
            Ssrc = ssrc;
        }

        /// <summary>
        /// Remove the padding and adjust the payload byte count accordingly.
        /// </summary>
        /// <param name="payload">The payload <see cref="ArraySegment{T}"/> associated with
        /// this packet.</param>
        public void RemovePadding(ref ArraySegment<byte> payload)
        {
            if (Padding)
            {
                int paddingOverhead = payload.Array[payload.Offset + payload.Count - 1];
                if (paddingOverhead <= payload.Count)
                {
                    payload = new ArraySegment<byte>(payload.Array, payload.Offset, payload.Count - paddingOverhead);
                    Padding = false;
                }
            }
        }

        internal static int FromBytes(ArraySegment<byte> input, out RtpFixedHeader header)
        {
            if (input.Count < FixedHeaderSize)
                throw new ArgumentOutOfRangeException("input", "Fixed header size underflow");

            int net32 = BitConverter.ToInt32(input.Array, input.Offset);
            int host32 = IPAddress.NetworkToHostOrder(net32);

            int sequence = (ushort)host32;
            BitVector32 flags = new BitVector32(host32 >> 16);

            net32 = BitConverter.ToInt32(input.Array, input.Offset + 4);
            int timeStamp = IPAddress.NetworkToHostOrder(net32);
            net32 = BitConverter.ToInt32(input.Array, input.Offset + 8);
            int ssrc = IPAddress.NetworkToHostOrder(net32);

            header = new RtpFixedHeader(flags, sequence, timeStamp, ssrc);
            return FixedHeaderSize;
        }
    }
}
