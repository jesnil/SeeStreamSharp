SeeStreamSharp
==============

This is a set of assemblies primarily targeting making recordings of video media
streams from network cameras.

The decision was to do an implementation purely using C# managed code. The
inherent asynchronous nature of network media streaming protocols is reflected
in how the code is utilizing the .Net 4.x support for composing asynchronous and
event-based code. The results from server operations and requested media stream
data is returned using `System.Threading.Tasks.Task<T>` and
`System.IObservable<T>`.

For network media streaming the only client currently implemented is RTSP.

Retrieved media streams are written to an ISO Based Media container using the
MP4 file extension. This file format is based on Apple Quick Time Movie. It was
selected partly as it is the natural container for the H.264 video codec
used by many network cameras. In addition an MP4 file supports including the
decoding time of the individual frames of the video stream. This is an advantage
when recording a video stream from a camera. One way a camera can adapt to e.g.
changing light conditions is to instantly change the video frame rate.

Refer to the SeeStreamSharp [WiKi](https://gitlab.com/jesnil/SeeStreamSharp/wikis/home)
for the usage documentation including code examples.

The SeeStreamSharp code is made available using the terms and conditions in
[Apache License 2.0](https://gitlab.com/jesnil/SeeStreamSharp/blob/master/LICENSE).
