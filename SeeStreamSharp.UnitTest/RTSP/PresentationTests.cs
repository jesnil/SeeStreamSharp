﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeeStreamSharp.RTSP.Tests
{
    [TestClass()]
    public class PresentationTests
    {
        [TestInitialize()]
        public void Init()
        {
            Presentation.Init();
        }

        //  This is a long running (~21 second) test.
        //[TestMethod()]
        public async Task PresentationConnectTimeOutTest()
        {
            //  This IP address should not have any responding computer.
            Uri presentationUrl = new Uri("rtsp://192.168.255.1/mpeg4/media.amp", UriKind.Absolute);

            Presentation p = new Presentation(presentationUrl);
            try
            {
                SessionDescription sdp = await p.GetSessionDescriptionAsync();

                //  This test is not supposed to get beyond above method call!
                Assert.Fail();
            }
            catch (AggregateException ae)
            {
                ae.Handle(e => {
                    SocketException sEx = e as SocketException;
                    if (sEx != null)
                    {
                        if (sEx.SocketErrorCode == SocketError.TimedOut)
                            return true;
                    }
                    return false;
                });
            }
        }

        [TestMethod()]
        public async Task PresentationConnectionRefusedFailTest()
        {
            //  This IP address should not have a responding RTSP server!
            Uri presentationUrl = new Uri("rtsp://localhost/mpeg4/media.amp", UriKind.Absolute);

            Presentation p = new Presentation(presentationUrl);
            try
            {
                SessionDescription sdp = await p.GetSessionDescriptionAsync();

                //  This test is not supposed to get beyond above method call!
                Assert.Fail();
            }
            catch (AggregateException ae)
            {
                ae.Handle(e => {
                    SocketException sEx = e as SocketException;
                    if (sEx != null)
                    {
                        if (sEx.SocketErrorCode == SocketError.ConnectionRefused)
                            return true;
                    }
                    return false;
                });
            }
        }

        [TestMethod()]
        public async Task PresentationResolvFailTest()
        {
            //  This fully qualified host name should not exist!
            Uri presentationUrl = new Uri("rtsp://non-existent.tld/mpeg4/media.amp", UriKind.Absolute);

            Presentation p = new Presentation(presentationUrl);
            try
            {
                SessionDescription sdp = await p.GetSessionDescriptionAsync();

                //  This test is not supposed to get beyond above method call!
                Assert.Fail();
            }
            catch (AggregateException ae)
            {
                ae.Flatten().Handle(e => {
                    SocketException sEx = e as SocketException;
                    if (sEx != null)
                    {
                        if (sEx.SocketErrorCode == SocketError.HostNotFound)
                            return true;
                    }
                    return false;
                });
            }
        }

        /// <summary>
        /// Test session description processing.
        /// </summary>
        [TestMethod()]
        public async Task PresentationAuthenticationTest()
        {
            AuthenticationConnection con = new AuthenticationConnection(this);
            ClientConnectionFactory.SetPrecreatedConnection(con);

            Presentation p = new Presentation(con._presentationUrl);

            try
            {
                SessionDescription sdp = await p.GetSessionDescriptionAsync();
                Assert.Fail("This should throw exception");
            }
            catch (Exception ex)
            {
                Assert.AreEqual("DESCRIBE request got an '401' status code response, reason phrase = 'Unauthorized'", ex.Message);
            }

            Assert.IsTrue(con._describeRequests == 3);
            Assert.AreEqual("Digest username=\"root\",realm=\"CAMERA 1 Digest\",nonce=\"00000385Y8327603a5e13e88c158884e6c248ab0ab5cf1\",uri=\"rtsp://169.254.217.225/axis-media/media.amp\",response=\"2dddd44ceeac3b9afe7430ade40e0eff\"", con._authorization1, HeaderCollection.Authorization + " invalid");
            Assert.AreEqual("Digest username=\"root\",realm=\"CAMERA 1 Digest\",nonce=\"00000385Y8327603a5e13e88c158884e6c248ab0ab5cf2\",uri=\"rtsp://169.254.217.225/axis-media/media.amp\",response=\"6a71e6138ca9da80bbb374924dc7a306\"", con._authorization2, HeaderCollection.Authorization + " invalid");
        }

        /// <summary>
        /// Test session description processing.
        /// </summary>
        [TestMethod()]
        public async Task PresentationSdpTest()
        {
            SdpConnection con = new SdpConnection(this);
            ClientConnectionFactory.SetPrecreatedConnection(con);

            Presentation p = new Presentation(con._presentationUrl);
            SessionDescription sdp = await p.GetSessionDescriptionAsync();
            if (con._failedMessage != null)
                Assert.Fail("After GetSessionDescriptionAsync():" + con._failedMessage);

            await p.SetupAsync(sdp.MediaDescriptions
                .Where(m => { return m.MediaType == SessionDescription.MediaType.video; })
                .First());
            if (con._failedMessage != null)
                Assert.Fail("After SetupAsync():" + con._failedMessage);

            await p.PlayAsync();
            if (con._failedMessage != null)
                Assert.Fail("After PlayAsync():" + con._failedMessage);
        }

        /// <summary>
        /// Test session description processing.
        /// </summary>
        [TestMethod()]
        public async Task PresentationSdp2Test()
        {
            Sdp2TestConnection con = new Sdp2TestConnection(this);
            ClientConnectionFactory.SetPrecreatedConnection(con);

            Presentation p = new Presentation(con._presentationUrl);
            SessionDescription sdp = await p.GetSessionDescriptionAsync();
            if (con._failedMessage != null)
                Assert.Fail("After GetSessionDescriptionAsync():" + con._failedMessage);

            await p.SetupAsync(sdp.MediaDescriptions
                .Where(m => { return m.MediaType == SessionDescription.MediaType.video; })
                .First());
            if (con._failedMessage != null)
                Assert.Fail("After SetupAsync():" + con._failedMessage);

            await p.PlayAsync();
            if (con._failedMessage != null)
                Assert.Fail("After PlayAsync():" + con._failedMessage);
        }

        /// <summary>
        /// Test session description result having a "Content-Base" header to be used for
        /// SETUP only.
        /// </summary>
        [TestMethod()]
        public async Task PresentationContentBaseTest()
        {
            ContentBaseTestConnection con = new ContentBaseTestConnection(this);
            ClientConnectionFactory.SetPrecreatedConnection(con);

            Presentation p = new Presentation(con._presentationUrl);
            SessionDescription sdp = await p.GetSessionDescriptionAsync();
            if (con._failedMessage != null)
                Assert.Fail("After GetSessionDescriptionAsync():" + con._failedMessage);

            await p.SetupAsync(sdp.MediaDescriptions
                .Where(m => { return m.MediaType == SessionDescription.MediaType.video; })
                .First());
            if (con._failedMessage != null)
                Assert.Fail("After SetupAsync():" + con._failedMessage);

            await p.PlayAsync();
            if (con._failedMessage != null)
                Assert.Fail("After PlayAsync():" + con._failedMessage);
        }

        /// <summary>
        /// Test session description processing.
        /// </summary>
        [TestMethod()]
        public async Task PresentationSdpMultiMediaTest()
        {
            SdpMultiMediaConnection con = new SdpMultiMediaConnection(this);
            ClientConnectionFactory.SetPrecreatedConnection(con);

            Presentation p = new Presentation(con._presentationUrl);
            SessionDescription sdp = await p.GetSessionDescriptionAsync();
            if (con._failedMessage != null)
                Assert.Fail("After GetSessionDescriptionAsync():" + con._failedMessage);

            Task<Session>[] sessions = new Task<Session>[2];

            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Requesting video");
            sessions[0] = p.SetupAsync(sdp.MediaDescriptions
                .Where(m => { return m.MediaType == SessionDescription.MediaType.video; })
                .First());
            if (con._failedMessage != null)
                Assert.Fail("After SetupAsync(video):" + con._failedMessage);

            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Sleeping");
            Thread.Sleep(100);

            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Requesting audio");
            sessions[1] = p.SetupAsync(sdp.MediaDescriptions
                .Where(m => { return m.MediaType == SessionDescription.MediaType.audio; })
                .First());
            if (con._failedMessage != null)
                Assert.Fail("After SetupAsync(video):" + con._failedMessage);

            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Start WaitAll()");
            await Task.WhenAll(sessions);
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Finished WaitAll()");

            if (con._failedMessage != null)
                Assert.Fail("After WaitAll():" + con._failedMessage);

            await p.PlayAsync();
            if (con._failedMessage != null)
                Assert.Fail("After PlayAsync():" + con._failedMessage);
        }
    }

    public class AuthenticationConnection : IClientConnection
    {
        internal readonly Uri _presentationUrl = new Uri("rtsp://root:root@169.254.217.225/axis-media/media.amp", UriKind.Absolute);

        internal int _describeRequests;
        internal string _authorization1;
        internal string _authorization2;

        private readonly PresentationTests _controller;

        public AuthenticationConnection(PresentationTests controller)
        {
            _controller = controller;
        }

        void IClientConnection.AddChannel(int channelNumber, IObserver<ArraySegment<byte>> receiver)
        {
            Debug.WriteLine("AddChannel");
        }

        int IClientConnection.ReserveEvenChannelNumber()
        {
            Debug.WriteLine("ReserveEvenChannelNumber");
            return 0;
        }

        void IClientConnection.Send(Request request)
        {
            //  This is implementation identical to the actual ClientConnection implementation!
            if (request.AuthenticationProvider != null)
                request.AuthenticationProvider.AddAuthorization(request, this);
            request.SetCSeq(_describeRequests + 1);

            Debug.WriteLine("Send() called with the following request:");
            Debug.Write(request.ToString());

            Describe d = request as Describe;
            if (d != null)
            {
                _describeRequests++;

                string authorization = d.Headers.Get(HeaderCollection.Authorization);
                if (authorization == null)
                {
                    //  This will trigger a resend because the response contains a challenge.
                    Response r = new Response(HttpStatusCode.Unauthorized, "Unauthorized");
                    r.Headers.Add(HeaderCollection.WwwAuthenticate, "Digest realm=\"CAMERA 1 Digest\"");
                    r.Headers.Add(HeaderCollection.WwwAuthenticate, "nonce=\"00000385Y8327603a5e13e88c158884e6c248ab0ab5cf1\"");
                    r.Headers.Add(HeaderCollection.WwwAuthenticate, "Basic realm=\"CAMERA 1 Basic\"");

                    //  Have this run on another thread.
                    Task.Factory.StartNew(() =>
                    {
                        request.Process(r, null, this);
                    });
                }
                else
                {
                    if (d.CSeq == 2)
                    {
                        _authorization1 = authorization;

                        //  This will trigger a resend because the response contains an update to a stale challenge.
                        Response r = new Response(HttpStatusCode.Unauthorized, "Unauthorized");
                        r.Headers.Add(HeaderCollection.WwwAuthenticate, "Digest realm=\"CAMERA 1 Digest\"");
                        r.Headers.Add(HeaderCollection.WwwAuthenticate, "nonce=\"00000385Y8327603a5e13e88c158884e6c248ab0ab5cf2\"");
                        r.Headers.Add(HeaderCollection.WwwAuthenticate, "stale=TRUE");

                        //  Have this run on another thread.
                        Task.Factory.StartNew(() =>
                        {
                            request.Process(r, null, this);
                        });
                    }
                    if (d.CSeq == 3)
                    {
                        _authorization2 = authorization;

                        //  This will NOT trigger a resend as the repeated "stale=TRUE"
                        //  is interpreted as a possible endless loop.
                        Response r = new Response(HttpStatusCode.Unauthorized, "Unauthorized");
                        r.Headers.Add(HeaderCollection.WwwAuthenticate, "Digest realm=\"CAMERA 1 Digest\"");
                        r.Headers.Add(HeaderCollection.WwwAuthenticate, "nonce=\"00000385Y8327603a5e13e88c158884e6c248ab0ab5cf3\"");
                        r.Headers.Add(HeaderCollection.WwwAuthenticate, "stale=TRUE");

                        //  Have this run on another thread.
                        Task.Factory.StartNew(() =>
                        {
                            request.Process(r, null, this);
                        });
                    }
                }
            }

            Debug.WriteLine(string.Empty);
        }

        void IClientConnection.SendInterleavedData(int channel, IList<ArraySegment<byte>> list)
        {
            //  This should never be called!
            throw new NotImplementedException();
        }

        void IClientConnection.UnreserveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("UnreserveChannel");
        }

        void IClientConnection.RemoveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("RemoveChannel");
        }
    }

    public class SdpConnection : IClientConnection
    {
        internal readonly Uri _presentationUrl = new Uri("rtsp://192.168.70.252/mpeg4/media.amp", UriKind.Absolute);
        internal const string _setupUrl = "rtsp://192.168.70.252/mpeg4/media.amp/trackID=1";
        internal const string _sdpToProvide = @"v=0
o=- 1218949574973387 1218949574973400 IN IP4 192.168.70.252
s=Media Presentation
e=NONE
c=IN IP4 0.0.0.0
b=AS:8000
t=0 0
a=control:*
a=range:npt=now-
a=mpeg4-iod: " +
"\"data:application/mpeg4-iod;base64,AoDUAE8BAf/1AQOAbwABQFBkYXRhOmFwcGxpY2F0aW9uL21wZWc0LW9kLWF1O2Jhc2U2NCxBUjBCR3dVZkF4Y0F5U1FBWlFRTklCRUVrK0FBZWhJQUFIb1NBQVlCQkE9PQQNAQUABAAAAAAAAAAAAAYJAQAAAAAAAAAAAzoAAkA2ZGF0YTphcHBsaWNhdGlvbi9tcGVnNC1iaWZzLWF1O2Jhc2U2NCx3QkFTWVFTSVVFVUZQd0E9BBICDQAAAgAAAAAAAAAABQMAAEAGCQEAAAAAAAAAAA==\"" +
@"
m=video 0 RTP/AVP 96
b=AS:8000
a=framerate:30.0
a=control:trackID=1
a=rtpmap:96 MP4V-ES/90000
a=fmtp:96 profile-level-id=245; config=000001B0F5000001B509000001000000012008D48D88032514043C14440F
a=mpeg4-esid:201
";

        internal string _failedMessage;

        private readonly PresentationTests _controller;

        public SdpConnection(PresentationTests controller)
        {
            _controller = controller;
        }

        void IClientConnection.AddChannel(int channelNumber, IObserver<ArraySegment<byte>> receiver)
        {
            Debug.WriteLine("AddChannel");
        }

        int IClientConnection.ReserveEvenChannelNumber()
        {
            Debug.WriteLine("ReserveEvenChannelNumber");
            return 0;
        }

        void IClientConnection.Send(Request request)
        {
            Debug.WriteLine("Send() called with the following request:");
            Debug.Write(request.ToString());

            Describe d = request as Describe;
            if (d != null)
            {
                Debug.WriteLine("d.Url = " + d.Url);

                if (d.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");
                r.Headers.ContentTypeValue = new System.Net.Mime.ContentType("application/sdp");
                r.Entity = Encoding.UTF8.GetBytes(_sdpToProvide);

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Setup s = request as Setup;
            if (s != null)
            {
                Debug.WriteLine("s.Url = " + s.Url);

                if (_setupUrl.Equals(s.Url.ToString()) == false)
                    _failedMessage = s.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");
                r.Headers.Add(HeaderCollection.Session, "14043071");
                r.Headers.Add(HeaderCollection.Session, "timeout=60");
                r.Headers.Add(HeaderCollection.Transport, "RTP/AVP/TCP");
                r.Headers.Add(HeaderCollection.Transport, "interleaved=0-1");
                r.Headers.Add(HeaderCollection.Transport, "ssrc=81E440BC");
                r.Headers.Add(HeaderCollection.Transport, "mode=\"PLAY\"");

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Play p = request as Play;
            if (p != null)
            {
                Debug.WriteLine("p.Url = " + p.Url);

                if (p.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Debug.WriteLine(string.Empty);
        }

        void IClientConnection.SendInterleavedData(int channel, IList<ArraySegment<byte>> list)
        {
            //  This should never be called!
            throw new NotImplementedException();
        }

        void IClientConnection.UnreserveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("UnreserveChannel");
        }

        void IClientConnection.RemoveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("RemoveChannel");
        }
    }

    public class Sdp2TestConnection : IClientConnection
    {
        internal readonly Uri _presentationUrl = new Uri("rtsp://192.168.1.55/axis-media/media.amp?fps=20&resolution=800x600", UriKind.Absolute);
        internal const string _setupUrl = "rtsp://192.168.1.55/axis-media/media.amp/trackID=1?fps=20&resolution=800x600";
        internal const string _sdpToProvide = @"v=0
o=- 1418374838887877 1418374838887877 IN IP4 192.168.1.55
s=Media Presentation
e=NONE
c=IN IP4 0.0.0.0
b=AS:50000
t=0 0
a=control:rtsp://192.168.1.55/axis-media/media.amp?fps=20&resolution=800x600
a=range:npt=0.000000-
m=video 0 RTP/AVP 96
b=AS:50000
a=framerate:20.0
a=control:rtsp://192.168.1.55/axis-media/media.amp/trackID=1?fps=20&resolution=800x600
a=rtpmap:96 H264/90000
a=fmtp:96 packetization-mode=1; profile-level-id=420029; sprop-parameter-sets=Z0IAKeKQGQJvy4C3AQEBpB4kRUA=,aM48gA==";

        internal string _failedMessage;

        private readonly PresentationTests _controller;

        public Sdp2TestConnection(PresentationTests controller)
        {
            _controller = controller;
        }

        void IClientConnection.AddChannel(int channelNumber, IObserver<ArraySegment<byte>> receiver)
        {
            Debug.WriteLine("AddChannel");
        }

        int IClientConnection.ReserveEvenChannelNumber()
        {
            Debug.WriteLine("ReserveEvenChannelNumber");
            return 0;
        }

        void IClientConnection.Send(Request request)
        {
            Debug.WriteLine("Send() called with the following request:");
            Debug.Write(request.ToString());

            Describe d = request as Describe;
            if (d != null)
            {
                Debug.WriteLine("d.Url = " + d.Url);

                if (d.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");
                r.Headers.ContentTypeValue = new System.Net.Mime.ContentType("application/sdp");
                r.Entity = Encoding.UTF8.GetBytes(_sdpToProvide);

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Setup s = request as Setup;
            if (s != null)
            {
                Debug.WriteLine("s.Url = " + s.Url);

                if (_setupUrl.Equals(s.Url.ToString()) == false)
                    _failedMessage = s.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");
                r.Headers.Add(HeaderCollection.Session, "14043071");
                r.Headers.Add(HeaderCollection.Session, "timeout=60");
                r.Headers.Add(HeaderCollection.Transport, "RTP/AVP/TCP");
                r.Headers.Add(HeaderCollection.Transport, "interleaved=0-1");
                r.Headers.Add(HeaderCollection.Transport, "ssrc=81E440BC");
                r.Headers.Add(HeaderCollection.Transport, "mode=\"PLAY\"");

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Play p = request as Play;
            if (p != null)
            {
                Debug.WriteLine("p.Url = " + p.Url);

                if (p.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Debug.WriteLine(string.Empty);
        }

        void IClientConnection.SendInterleavedData(int channel, IList<ArraySegment<byte>> list)
        {
            //  This should never be called!
            throw new NotImplementedException();
        }

        void IClientConnection.UnreserveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("UnreserveChannel");
        }

        void IClientConnection.RemoveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("RemoveChannel");
        }
    }

    public class ContentBaseTestConnection : IClientConnection
    {
        internal readonly Uri _presentationUrl = new Uri("rtsp://rtsp.com/tvchannel_v3_1/332_110.sdp", UriKind.Absolute);
        internal const string _contentBase = "rtsp://rtsp.com/stream332_110.sdp/";
        internal const string _setupUrl = "rtsp://rtsp.com/stream332_110.sdp/track2";
        internal const string _sdpToProvide = @"v=0
o=- 1479585133819443 1 IN IP4 0.0.0.0
s=332_110.sdp
i=TV-channel
t=0 0
a=control:*
a=range:npt=now-
m=audio 0 RTP/AVP 96
c=IN IP4 0.0.0.0
b=AS:64
a=rtpmap:96 mpeg4-generic/32000
a=fmtp:96 streamtype=5;profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1288
a=control:track1
m=video 0 RTP/AVP 97
c=IN IP4 0.0.0.0
b=AS:235
a=rtpmap:97 H264/90000
a=fmtp:97 packetization-mode=1; profile-level-id=42C01E; sprop-parameter-sets=Z0LAHtsFB+hAAAADAEAAAAyDxYu4,aMqPIA==
a=control:track2
";

        internal string _failedMessage;

        private readonly PresentationTests _controller;

        public ContentBaseTestConnection(PresentationTests controller)
        {
            _controller = controller;
        }

        void IClientConnection.AddChannel(int channelNumber, IObserver<ArraySegment<byte>> receiver)
        {
            Debug.WriteLine("AddChannel");
        }

        int IClientConnection.ReserveEvenChannelNumber()
        {
            Debug.WriteLine("ReserveEvenChannelNumber");
            return 2;
        }

        void IClientConnection.Send(Request request)
        {
            Debug.WriteLine("Send() called with the following request:");
            Debug.Write(request.ToString());

            Describe d = request as Describe;
            if (d != null)
            {
                Debug.WriteLine("d.Url = " + d.Url);

                if (d.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");
                r.Headers.Add(HeaderCollection.ContentBase, _contentBase);
                r.Headers.ContentTypeValue = new System.Net.Mime.ContentType("application/sdp");
                r.Entity = Encoding.UTF8.GetBytes(_sdpToProvide);

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Setup s = request as Setup;
            if (s != null)
            {
                Debug.WriteLine("s.Url = " + s.Url);

                if (_setupUrl.Equals(s.Url.ToString()) == false)
                    _failedMessage = s.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");
                r.Headers.Add(HeaderCollection.Session, "14043071");
                r.Headers.Add(HeaderCollection.Session, "timeout=60");
                r.Headers.Add(HeaderCollection.Transport, "RTP/AVP/TCP");
                r.Headers.Add(HeaderCollection.Transport, "interleaved=0-1");
                r.Headers.Add(HeaderCollection.Transport, "ssrc=81E440BC");
                r.Headers.Add(HeaderCollection.Transport, "mode=\"PLAY\"");

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Play p = request as Play;
            if (p != null)
            {
                Debug.WriteLine("p.Url = " + p.Url);

                if (p.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                });
            }

            Debug.WriteLine(string.Empty);
        }

        void IClientConnection.SendInterleavedData(int channel, IList<ArraySegment<byte>> list)
        {
            //  This should never be called!
            throw new NotImplementedException();
        }

        void IClientConnection.UnreserveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("UnreserveChannel");
        }

        void IClientConnection.RemoveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("RemoveChannel");
        }
    }

    public class SdpMultiMediaConnection : IClientConnection
    {
        internal readonly Uri _presentationUrl = new Uri("rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov", UriKind.Absolute);
        internal const string _setup1Url = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov/trackID=1";
        internal const string _setup2Url = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov/trackID=2";
        internal const string _sdpToProvide = @"v=0
o=- 1649242139 1649242139 IN IP4 184.72.239.149
s=BigBuckBunny_115k.mov
c=IN IP4 184.72.239.149
t=0 0
a=sdplang:en
a=range:npt=0- 596.48
a=control:*
m=audio 0 RTP/AVP 96
a=rtpmap:96 mpeg4-generic/12000/2
a=fmtp:96 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1490
a=control:trackID=1
m=video 0 RTP/AVP 97
a=rtpmap:97 H264/90000
a=fmtp:97 packetization-mode=1;profile-level-id=42C01E;sprop-parameter-sets=Z0LAHtkDxWhAAAADAEAAAAwDxYuS,aMuMsg==
a=cliprect:0,0,160,240
a=framesize:97 240-160
a=framerate:24.0
a=control:trackID=2
";

        internal string _failedMessage;
        private List<int> _channels = new List<int>();

        private readonly PresentationTests _controller;

        public SdpMultiMediaConnection(PresentationTests controller)
        {
            _controller = controller;
        }

        void IClientConnection.AddChannel(int channelNumber, IObserver<ArraySegment<byte>> receiver)
        {
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": AddChannel called " + channelNumber);
            _channels.Add(channelNumber);
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": AddChannel exited " + _channels.Count);
        }

        int IClientConnection.ReserveEvenChannelNumber()
        {
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": ReserveEvenChannelNumber called");
            int even_number = _channels.Count;
            _channels.Add(even_number);
            _channels.Add(even_number + 1);
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": ReserveEvenChannelNumber exited " + _channels.Count);
            return even_number;
        }

        private const string TransportProtocol = "RTP/AVP/TCP";
        private const string SessionValue = "A1B2C3D4";

        void IClientConnection.Send(Request request)
        {
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Send() called with the following request:");
            Debug.Write(request.ToString());

            Describe d = request as Describe;
            if (d != null)
            {
                Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": d.Url = " + d.Url);

                if (d.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");
                r.Headers.ContentTypeValue = new System.Net.Mime.ContentType("application/sdp");
                r.Entity = Encoding.UTF8.GetBytes(_sdpToProvide);

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    request.Process(r, null, this);
                    Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Describe response processed");
                });
            }

            Setup s = request as Setup;
            if (s != null)
            {
                Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": s.Url = " + s.Url);
                Response r = new Response(HttpStatusCode.OK, "OK");
                int delay = 0;

                if (_setup1Url.Equals(s.Url.ToString()) == true)
                {
                    //  This is to be set up second.
                    string session = s.Headers.Get(HeaderCollection.Session);
                    if (session == null)
                        _failedMessage = "SETUP missing Session: " + s.Url;
                    else
                    {
                        if (!session.Equals(SessionValue, StringComparison.Ordinal))
                        {
                            _failedMessage = "SETUP has unexpected SessionValue: " + s.Url;
                            return;
                        }
                        r.Headers.Add(HeaderCollection.Session, session);
                    }

                    r.Headers.Add(HeaderCollection.Transport, TransportProtocol);
                    r.Headers.Add(HeaderCollection.Transport, "interleaved=12-13");
                    r.Headers.Add(HeaderCollection.Transport, "ssrc=C1E440B0");
                    r.Headers.Add(HeaderCollection.Transport, "mode=\"PLAY\"");

                    delay = 250;
                }
                else if (_setup2Url.Equals(s.Url.ToString()) == true)
                {
                    //  This is to be set up first.
                    string session = s.Headers.Get(HeaderCollection.Session);
                    if (session != null)
                        _failedMessage = "SETUP has unexpected Session: " + s.Url;

                    r.Headers.Add(HeaderCollection.Session, SessionValue);
                    r.Headers.Add(HeaderCollection.Session, "timeout=60");
                    r.Headers.Add(HeaderCollection.Transport, TransportProtocol);
                    r.Headers.Add(HeaderCollection.Transport, "interleaved=14-15");
                    r.Headers.Add(HeaderCollection.Transport, "ssrc=81E440BC");
                    r.Headers.Add(HeaderCollection.Transport, "mode=\"PLAY\"");

                    delay = 500;
                }
                else
                {
                    _failedMessage = "Unexpected SETUP request" + s.Url;
                    return;
                }

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(delay);
                    Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Response for SETUP Url = " + s.Url);
                    request.Process(r, null, this);
                    Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Setup response processed");
                });
            }

            Play p = request as Play;
            if (p != null)
            {
                Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": p.Url = " + p.Url);

                if (p.Url.Equals(_presentationUrl) == false)
                    _failedMessage = d.Url.OriginalString;

                Response r = new Response(HttpStatusCode.OK, "OK");

                //  Have this run on another thread.
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(100);
                    request.Process(r, null, this);
                    Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": Play response processed");
                });
            }

            Debug.WriteLine(string.Empty);
        }

        void IClientConnection.SendInterleavedData(int channel, IList<ArraySegment<byte>> list)
        {
            //  This should never be called!
            throw new NotImplementedException();
        }

        void IClientConnection.UnreserveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": UnreserveChannel called " + reservedChannelNumber);

            if (_channels.Contains(reservedChannelNumber))
                _channels.Remove(reservedChannelNumber);
            else
                _failedMessage = "Unreserving non-reserved " + reservedChannelNumber;
            if (_channels.Contains(reservedChannelNumber + 1))
                _channels.Remove(reservedChannelNumber + 1);
            else
                _failedMessage = "Unreserving non-reserved +1 " + reservedChannelNumber;
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff") + ", thread=" + Thread.CurrentThread.ManagedThreadId + ": UnreserveChannel " + _channels.Count);
        }

        void IClientConnection.RemoveChannel(int reservedChannelNumber)
        {
            Debug.WriteLine("RemoveChannel");
        }
    }
}