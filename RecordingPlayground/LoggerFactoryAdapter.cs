﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using Common.Logging.Factory;
using System;
using System.Collections.Generic;

namespace RecordingPlayground
{
    internal class LoggerFactoryAdapter : AbstractCachingLoggerFactoryAdapter
    {
        private readonly Dictionary<string, LogLevel> _settings = new Dictionary<string, LogLevel>();

        public LoggerFactoryAdapter()
        {
            _settings.Add("SeeStreamSharp.RTSP.Presentation", LogLevel.Info);
            _settings.Add("SeeStreamSharp.RTSP.ClientConnection", LogLevel.All);
            _settings.Add("SeeStreamSharp.RTSP.ReceiveBuffer", LogLevel.Debug);
            _settings.Add("SeeStreamSharp.Media.IsoBasedContainer.Avc1TrackWriter", LogLevel.Info);
            _settings.Add("SeeStreamSharp.Media.RtpStreamSink.H264FrameBuffer", LogLevel.Info);
            _settings.Add("SeeStreamSharp.Media.RtpStreamSink.Mpeg4AacHbrFrameBuffer", LogLevel.Info);
        }

        protected override ILog CreateLogger(string name)
        {
            LogLevel level;
            if (!_settings.TryGetValue(name, out level))
                level = LogLevel.All;
            return new Logger(name, level);
        }

        public void ClearCachedLoggers()
        {
            ClearLoggerCache();
        }
    }

    internal class Logger : AbstractLogger
    {
        private readonly string _name;
        private readonly LogLevel _level;

        public Logger(string name, LogLevel level)
        {
            _name = name;
            _level = level;
        }

        public override bool IsDebugEnabled
        {
            get { return (_level <= LogLevel.Debug); }
        }

        public override bool IsErrorEnabled
        {
            get { return (_level <= LogLevel.Error); }
        }

        public override bool IsFatalEnabled
        {
            get { return (_level <= LogLevel.Fatal); }
        }

        public override bool IsInfoEnabled
        {
            get { return (_level <= LogLevel.Info); }
        }

        public override bool IsTraceEnabled
        {
            get { return (_level <= LogLevel.Trace); }
        }

        public override bool IsWarnEnabled
        {
            get { return (_level <= LogLevel.Warn); }
        }

        protected override void WriteInternal(LogLevel level, object message, Exception exception)
        {
            Form1.AddLogMessage(_name, level, message, exception);
        }
    }
}
