﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.RTP;
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace SeeStreamSharp.Media.RtpStreamSink
{
    /// <summary>
    /// HEVC (H.265) RTP payload to NAL unit buffer converter.
    /// </summary>
    /// <remarks>
    /// Processes received RTP packets as documented in RFC 7798 RTP Payload Format for
    /// High Efficiency Video Coding (HEVC) (https://www.rfc-editor.org/rfc/rfc7798).
    /// </remarks>
    internal sealed class H265FrameBuffer : H26xFrameBuffer
    {
        private const int NAL_UNIT_SIZE = 2;
        private const int FU_HEADER_SIZE = 1;

        //private const int NAL_UNIT_CODED_SLICE_TRAIL_N = 0;
        //private const int NAL_UNIT_CODED_SLICE_TRAIL_R = 1;
        //private const int NAL_UNIT_CODED_SLICE_TSA_N = 2;
        //private const int NAL_UNIT_CODED_SLICE_TSA_R = 3;
        //private const int NAL_UNIT_CODED_SLICE_STSA_N = 4;
        //private const int NAL_UNIT_CODED_SLICE_STSA_R = 5;
        //private const int NAL_UNIT_CODED_SLICE_RADL_N = 6;
        //private const int NAL_UNIT_CODED_SLICE_RADL_R = 7;
        //private const int NAL_UNIT_CODED_SLICE_RASL_N = 8;
        //private const int NAL_UNIT_CODED_SLICE_RASL_R = 9;
        //private const int NAL_UNIT_RESERVED_VCL_N10 = 10;
        //private const int NAL_UNIT_RESERVED_VCL_R11 = 11;
        //private const int NAL_UNIT_RESERVED_VCL_N12 = 12;
        //private const int NAL_UNIT_RESERVED_VCL_R13 = 13;
        //private const int NAL_UNIT_RESERVED_VCL_N14 = 14;
        //private const int NAL_UNIT_RESERVED_VCL_R15 = 15;
        private const int NAL_UNIT_CODED_SLICE_BLA_W_LP = 16;
        private const int NAL_UNIT_CODED_SLICE_BLA_W_RADL = 17;
        private const int NAL_UNIT_CODED_SLICE_BLA_N_LP = 18;
        private const int NAL_UNIT_CODED_SLICE_IDR_W_RADL = 19;
        private const int NAL_UNIT_CODED_SLICE_IDR_N_LP = 20;
        private const int NAL_UNIT_CODED_SLICE_CRA = 21;
        //private const int NAL_UNIT_RESERVED_IRAP_VCL22 = 22;
        //private const int NAL_UNIT_RESERVED_IRAP_VCL23 = 23;
        //private const int NAL_UNIT_RESERVED_VCL24 = 24;
        //private const int NAL_UNIT_RESERVED_VCL25 = 25;
        //private const int NAL_UNIT_RESERVED_VCL26 = 26;
        //private const int NAL_UNIT_RESERVED_VCL27 = 27;
        //private const int NAL_UNIT_RESERVED_VCL28 = 28;
        //private const int NAL_UNIT_RESERVED_VCL29 = 29;
        //private const int NAL_UNIT_RESERVED_VCL30 = 30;
        //private const int NAL_UNIT_RESERVED_VCL31 = 31;

        internal const int NAL_UNIT_VPS = 32;
        internal const int NAL_UNIT_SPS = 33;
        internal const int NAL_UNIT_PPS = 34;
        //private const int NAL_UNIT_ACCESS_UNIT_DELIMITER = 35;
        //private const int NAL_UNIT_EOS = 36;
        //private const int NAL_UNIT_EOB = 37;
        //private const int NAL_UNIT_FILLER_DATA = 38;
        //private const int NAL_UNIT_PREFIX_SEI = 39;
        //private const int NAL_UNIT_SUFFIX_SEI = 40;
        //private const int NAL_UNIT_RESERVED_NVCL41 = 41;
        //private const int NAL_UNIT_RESERVED_NVCL42 = 42;
        //private const int NAL_UNIT_RESERVED_NVCL43 = 43;
        //private const int NAL_UNIT_RESERVED_NVCL44 = 44;
        //private const int NAL_UNIT_RESERVED_NVCL45 = 45;
        //private const int NAL_UNIT_RESERVED_NVCL46 = 46;
        //private const int NAL_UNIT_RESERVED_NVCL47 = 47;

        //  NAL UNIT 48 and above is not to be forwarded to a decoder (not stored in the media file).
        private const int NAL_UNIT_AGGREGATION_PACKET = 48;    //  RFC 7798
        private const int NAL_UNIT_FRAGMENTATION_UNIT = 49;    //  RFC 7798
        private const int NAL_UNIT_PACI_PACKET = 50;           //  RFC 7798

        //private const int NAL_UNIT_UNSPECIFIED_51 = 51;
        //private const int NAL_UNIT_UNSPECIFIED_52 = 52;
        //private const int NAL_UNIT_UNSPECIFIED_53 = 53;
        //private const int NAL_UNIT_UNSPECIFIED_54 = 54;
        //private const int NAL_UNIT_UNSPECIFIED_55 = 55;
        //private const int NAL_UNIT_UNSPECIFIED_56 = 56;
        //private const int NAL_UNIT_UNSPECIFIED_57 = 57;
        //private const int NAL_UNIT_UNSPECIFIED_58 = 58;
        //private const int NAL_UNIT_UNSPECIFIED_59 = 59;
        //private const int NAL_UNIT_UNSPECIFIED_60 = 60;
        //private const int NAL_UNIT_UNSPECIFIED_61 = 61;
        //private const int NAL_UNIT_UNSPECIFIED_62 = 62;
        //private const int NAL_UNIT_UNSPECIFIED_63 = 63;

        //  NAL unit word.
        private static readonly BitVector32.Section s_tId = BitVector32.CreateSection(7);
        private static readonly BitVector32.Section s_layerId = BitVector32.CreateSection(63, s_tId);
        internal static readonly BitVector32.Section s_nalType = BitVector32.CreateSection(63, s_layerId);
        private static readonly BitVector32.Section s_forbiddenZero = BitVector32.CreateSection(1, s_nalType);

        //  FU header
        private readonly int _donlSize;
        private static readonly BitVector32.Section s_fuType = BitVector32.CreateSection(63);
        private static readonly BitVector32.Section s_fuEnd = BitVector32.CreateSection(1, s_fuType);
        private static readonly BitVector32.Section s_fuStart = BitVector32.CreateSection(1, s_fuEnd);

        private IH264Consumer _H264Consumer;

        public H265FrameBuffer(int maxDonDiff)
            : base(LogManager.GetLogger<H265FrameBuffer>())
        {
            //  A singel NAL packet may have the NAL unit word followed by a "DONL".
            _donlSize = maxDonDiff > 0 ? 2 : 0;
        }

        public override void Connect(IFrameBufferConsumer consumer, Session session)
        {
            base.Connect(consumer, session);
            _H264Consumer = consumer as IH264Consumer;
        }

        protected override void DoNext(Packet value)
        {
            //  H.265 payload requires a non-padded byte count.
            value.Header.RemovePadding(ref value.Payload);

            //  The H.265 payload has a 2 byte NAL unit header.
            if (value.Payload.Count < NAL_UNIT_SIZE)
                return;

            BitVector32 nalUnitType = new BitVector32(IPAddress.NetworkToHostOrder(BitConverter.ToInt16(value.Payload.Array, value.Payload.Offset)));

            int nalUnit = nalUnitType[s_nalType];
            //Debug.WriteLine($"forbiddenZero={nalUnitType[s_forbiddenZero]}, nalUnit={nalUnit}, layerId={nalUnitType[s_layerId]}, tId={nalUnitType[s_tId]}");

            if (nalUnit < NAL_UNIT_AGGREGATION_PACKET)
            {
                //  Calculate the buffer size compensating for a possible "DONL" that is not to be stored.
                byte[] buffer = new byte[value.Payload.Count - _donlSize];
                //  Copy the NAL unit header.
                Array.Copy(value.Payload.Array, value.Payload.Offset,
                    buffer, 0, NAL_UNIT_SIZE);
                //  Copy the NAL unit payload.
                int startOffset = NAL_UNIT_SIZE + _donlSize;
                Array.Copy(value.Payload.Array, value.Payload.Offset + startOffset,
                    buffer, NAL_UNIT_SIZE, value.Payload.Count - startOffset);

                FragmentFlags flags = FragmentFlags.EndOfNal;
                flags |= IsKeyFrame(nalUnit) ? FragmentFlags.SyncFrame : 0;
                flags |= value.Header.Marker ? FragmentFlags.Marker : 0;
                ConsecutiveSequenceBasedAdd(value.Header.Sequence, value.Header.TimeStamp, flags, buffer);

                //  The HVC1 track writer can store if additional parameter sets has been
                //  observed in the stream of NAL units.
                if (nalUnit >= NAL_UNIT_VPS && nalUnit <= NAL_UNIT_PPS)
                {
                    _H264Consumer.AddToParameterSets(buffer, 0, buffer.Length);
                }
            }
            else if (nalUnit == NAL_UNIT_AGGREGATION_PACKET)
            {
                //  TODO: Aggregation packets.
                Debug.WriteLine("NAL_UNIT_AGGREGATION_PACKET");

                FragmentFlags flags = FragmentFlags.EndOfNal;
                flags |= value.Header.Marker ? FragmentFlags.Marker : 0;

                byte[] buffer = ParseAggregationPacket(value.Payload.Array, value.Payload.Offset + _donlSize + NAL_UNIT_SIZE,
                    value.Payload.Count - _donlSize - NAL_UNIT_SIZE, ref flags);

                ConsecutiveSequenceBasedAdd(value.Header.Sequence, value.Header.TimeStamp, flags, buffer);
            }
            else if (nalUnit == NAL_UNIT_FRAGMENTATION_UNIT)
            {
                //  The FU header follows immediately after the fragmentation payload header.
                BitVector32 fuHeader = new BitVector32(value.Payload.Array[value.Payload.Offset + NAL_UNIT_SIZE]);
                //Debug.WriteLine($"nalUnit = {nalUnit}, fuHeader = {fuHeader.Data:X}, start={fuHeader[s_fuStart]}, end={fuHeader[s_fuEnd]}, type={fuHeader[s_fuType]}");

                //  Assuming it is a start fragment payload it requires a buffer size of the packet
                //  minus a possible "DONL" and the extra FU header to store.
                int bufferSize = value.Payload.Count - _donlSize - FU_HEADER_SIZE;
                //  As for bufferSize it is assumed a start fragment payload, saving the original
                //  NAL unit type carried in the FU header.
                int destinationOffset = NAL_UNIT_SIZE;

                if (fuHeader[s_fuStart] == 0)
                {
                    //  Remove the fragmentation NAL unit header completly.
                    bufferSize -= NAL_UNIT_SIZE;
                    destinationOffset = 0;
                }

                byte[] buffer = new byte[bufferSize];
                //  Copy the NAL unit payload.
                int startOffset = NAL_UNIT_SIZE + _donlSize + FU_HEADER_SIZE;
                Array.Copy(value.Payload.Array, value.Payload.Offset + startOffset,
                    buffer, destinationOffset, value.Payload.Count - startOffset);

                FragmentFlags flags = FragmentFlags.FragmentedNal;
                if (fuHeader[s_fuStart] > 0)
                {
                    flags |= FragmentFlags.StartOfNalFragment;

                    //  Set the fragmentation unit type with the actual payload type as carried
                    //  in the FU header.
                    int fuNal = fuHeader[s_fuType];
                    nalUnitType[s_nalType] = fuNal;
                    //  Network byte order.
                    buffer[0] = (byte)(nalUnitType.Data >> 8);
                    buffer[1] = (byte)(nalUnitType.Data);

                    flags |= IsKeyFrame(fuNal) ? FragmentFlags.SyncFrame : 0;
                    //if (IsKeyFrame(fuNal))
                    //    Debug.WriteLine("***fuNal == NAL_UNIT_CODED_SLICE_IDR_N_LP***");
                }

                flags |= fuHeader[s_fuEnd] != 0 ? FragmentFlags.EndOfNal : 0;
                flags |= value.Header.Marker ? FragmentFlags.Marker : 0;
                ConsecutiveSequenceBasedAdd(value.Header.Sequence, value.Header.TimeStamp, flags, buffer);
            }
            else if (nalUnit == NAL_UNIT_PACI_PACKET)
            {
                _logger.WarnFormat("{0} Payload Content Information (50) not supported.", _loggingId);
            }
            //  Unexpected NAL unit that is not to be stored.
            else if (nalUnit > NAL_UNIT_PACI_PACKET)
            {
                _logger.WarnFormat("{0} Unexpected NAL unit (" + nalUnit + ").", _loggingId);
            }
        }

        private byte[] ParseAggregationPacket(byte[] buffer, int offset, int count, ref FragmentFlags flags)
        {
            //  Assume that full packet size plus some margin for 5 length fields that is going
            //  to be expanded to double size is a god start.
            BinaryWriter writer = new BinaryWriter(new MemoryStream(count + 10));

            while (count > 1)
            {
                //  First get the 16 bit length of the aggregated NAL unit.
                int length = (buffer[offset] << 8) + buffer[offset + 1];
                offset += 2;
                count -= 2;
                if (length > count)
                {
                    _logger.ErrorFormat("{0} STAP content lenght={1} but remaining bytes={2}",
                        _loggingId, length, count);
                    break;
                }

                writer.Write(IPAddress.HostToNetworkOrder(length));
                writer.Write(buffer, offset, length);

                //  Move to the start of next aggregated NAL unit.
                offset += length;
                count -= length;
            }

            count = (int)writer.BaseStream.Length;
            byte[] result = new byte[count];
            writer.Seek(0, SeekOrigin.Begin);
            writer.BaseStream.Read(result, 0, count);
            return result;
        }

        private static bool IsKeyFrame(int nalUnitType)
        {
            switch (nalUnitType)
            {
                case NAL_UNIT_CODED_SLICE_BLA_W_LP:
                case NAL_UNIT_CODED_SLICE_BLA_W_RADL:
                case NAL_UNIT_CODED_SLICE_BLA_N_LP:
                case NAL_UNIT_CODED_SLICE_IDR_W_RADL:
                case NAL_UNIT_CODED_SLICE_IDR_N_LP:
                case NAL_UNIT_CODED_SLICE_CRA:
                    return true;
                default:
                    return false;
            }
        }
    }
}
