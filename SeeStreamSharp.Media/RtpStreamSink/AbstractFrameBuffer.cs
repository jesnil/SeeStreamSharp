﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.RTP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace SeeStreamSharp.Media.RtpStreamSink
{
    [Flags]
    internal enum FragmentFlags
    {
        Marker = 1,
        SyncFrame = 2,

        //  Specific for H.264.
        EndOfNal = 4,
        StartOfNalFragment = 8,
        FragmentedNal = 16,
    }

    internal abstract class AbstractFrameBuffer : IObserver<Packet>
    {
        internal struct Fragment
        {
            public readonly int Id;
            public readonly uint Timestamp;
            public readonly FragmentFlags Flags;
            public readonly byte[] Payload;

            public Fragment(int timestamp, FragmentFlags flags, byte[] payload)
            {
                Id = 0;
                Timestamp = (uint)timestamp;
                Flags = flags;
                Payload = payload;
            }

            public Fragment(int id, int timestamp, FragmentFlags flags, byte[] payload)
            {
                Id = id;
                Timestamp = (uint)timestamp;
                Flags = flags;
                Payload = payload;
            }
        }

        protected readonly ILog _logger;
        protected readonly int _loggingId;
        protected readonly List<Fragment> _fragments = new List<Fragment>(29);

        //  To be able to separate a worker thread processing received stream data while
        //  at the same time another thread is attempting to stop the buffering.
        private readonly object _receiveLock = new object();

        private volatile IFrameBufferConsumer _consumer;
        private volatile IDisposable _rtpObservable;

        private bool _baseSequenceIsSet;
        private int _baseSequence;

        public AbstractFrameBuffer(ILog logger)
        {
            _logger = logger;
            _loggingId = RuntimeHelpers.GetHashCode(_receiveLock);

            //  When logging is disabled it should not cause any garbage (heap
            //  allocations) at all in the key code path.
            DoNextLogAction = new Action<FormatMessageHandler>(DoNextLog);
        }

        #region No heap allocation logging

        private readonly Action<FormatMessageHandler> DoNextLogAction;

        private Packet _logValue;

        private void DoNextLog(FormatMessageHandler fmh)
        {
            fmh("{0} Sequence={1}, TimeStamp={2}{3}marker={4} count={5}{6}payload={7}",
                _loggingId, _logValue.Header.Sequence, _logValue.Header.TimeStamp, Environment.NewLine,
                _logValue.Header.Marker, _logValue.Payload.Count, Environment.NewLine,
                BitConverter.ToString(_logValue.Payload.Array, _logValue.Payload.Offset,
                                      _logValue.Payload.Count > 25 ? 25 : _logValue.Payload.Count));
        }

        #endregion

        public virtual void Connect(IFrameBufferConsumer consumer, Session session)
        {
            _consumer = consumer;
            _rtpObservable = session.Subscribe(this);
        }

        /// <summary>
        /// Analyze a <see cref="Fragment"/> to determine if it is to be written as
        /// part of the chunk. Can add or insert additional data in <paramref name="content"/>
        /// </summary>
        /// <param name="content">List of track writer byte segments already to be written to the
        /// container</param>
        /// <param name="index">The index in the frame buffer fragments list that is about to be
        /// added to the <paramref name="content"/> list</param>
        /// <returns>
        /// If less than Zero, discard the current <paramref name="index"/>.
        /// If Zero, use the current <paramref name="index"/>
        /// If more than Zero, use the current <paramref name="index"/> and add the returned
        /// value to the calculated frame size.
        /// </returns>
        public virtual int ProcessFragmentConcatenation(IList<ArraySegment<byte>> content, int index)
        {
            Fragment fragment = _fragments[index];

            //  Missing packets is not to be added.
            if (fragment.Payload == null)
                return -1;

            //  Just add fragment as-is.
            return 0;
        }

        /// <summary>
        /// Unsubsribe to the RTP session and flush media data to the track.
        /// </summary>
        /// <remarks>
        /// This method can be called both by the track container to stop receiving additional
        /// media packets or because of an exception from the track. The later because the track
        /// may now be in an unstable state unable to process/store any more media data.
        /// </remarks>
        public void StopReceiving()
        {
            IFrameBufferConsumer consumer;

            lock (_receiveLock)
            {
                _rtpObservable?.Dispose();
                _rtpObservable = null;

                //  The first holder of the lock will get the _consumer object while every
                //  subsequent holders of the lock will get null.
                consumer = _consumer;
                _consumer = null;
            }

            //  This method may be called more than once so consumer may already be null!
            if (consumer != null)
            {
                //  This is a good location to catch all exceptions occuring on media data flush
                //  (writing to the file system) or other exceptions in the writer track code.
                //  Assumes that any exception in the frame buffer and track writer code indicates
                //  that they are now in an unstable state.
                //  However, we dont need to do anything here as we are already in StopReceiving()
                //  method.
                try
                {
                    consumer.FrameBufferFinalized(_fragments);
                }
                catch (Exception ex)
                {
                    _logger.Error("Frame buffer finalization exception: " + ex.Message);
                    _logger.Error("Stack trace:" + ex.StackTrace);
                }
            }
        }

        protected void ConsecutiveSequenceBasedAdd(int seqNumber, int timestamp, FragmentFlags flags, byte[] payload)
        {
            //  Has to be able to handle that the first packet received has seqNumber == 0.
            if (!_baseSequenceIsSet)
            {
                _baseSequence = seqNumber;
                _baseSequenceIsSet = true;
            }

            int index = seqNumber - _baseSequence;

            if (index >= _fragments.Count)
            {
                while (_fragments.Count < index)
                {
                    _fragments.Add(new Fragment());
                }
                _fragments.Add(new Fragment(timestamp, flags, payload));
            }
            else
                _fragments[index] = new Fragment(timestamp, flags, payload);

            NotifyConsumer(index);
        }

        protected void SortedNonConsecutiveAdd(int id, int timestamp, FragmentFlags flags, byte[] payload)
        {
            int index = _fragments.Count - 1;

            if (index == -1 || _fragments[index].Id < id)
            {
                _fragments.Add(new Fragment(id, timestamp, flags, payload));
                index++;
            }
            else
            {
                do
                {
                    index--;
                } while (index > 0 && _fragments[index].Id > id);

                _fragments.Insert(index, new Fragment(id, timestamp, flags, payload));
            }

            NotifyConsumer(index);
        }

        private void NotifyConsumer(int index)
        {
            IFrameBufferConsumer consumer = _consumer;
            int? consumed = consumer?.FrameBufferUpdated(_fragments, index);
            if (consumed > 0)
            {
                _baseSequence += consumed.Value;
                _fragments.RemoveRange(0, consumed.Value);
            }
        }

        /// <summary>
        /// Workaround to calculate constant frame duration from available frames.
        /// </summary>
        /// <returns>Frame duration</returns>
        /// <remarks>
        /// This code assumes that there are more than one frame in the frame buffer and
        /// that the first two frames are consecutive (no packet loss throwing of the frame
        /// calculation).
        /// </remarks>
        protected int TryCalculateConstantDuration()
        {
            int result = 0;

            int index = 0;
            while (index < _fragments.Count && (_fragments[index].Flags & FragmentFlags.Marker) == 0)
                index++;
            //  Index is now the last packet in the first frame.
            //  Move to the first packet in the next frame.
            index++;

            if (index < _fragments.Count)
            {
                long duration = _fragments[index].Timestamp;
                duration -= _fragments[0].Timestamp;

                if (duration < 0)
                    duration += uint.MaxValue;

                result = (int)duration;
            }

            return result;
        }

        #region IObserver<Packet>

        public void OnNext(Packet value)
        {
            if (_rtpObservable != null)
            {
                lock (_receiveLock)
                {
                    if (_rtpObservable != null)
                    {
                        _logValue = value;
                        _logger.Trace(DoNextLogAction);

                        Debug.WriteLine("Sequence=" + value.Header.Sequence +
                            ", TimeStamp=" + value.Header.TimeStamp +
                            ", marker=" + value.Header.Marker +
                            ", padding=" + value.Header.Padding +
                            ", payload=" + value.Payload.Offset +
                            ", count=" + value.Payload.Count +
                            ", payload=" + BitConverter.ToString(value.Payload.Array, value.Payload.Offset,
                                                                 value.Payload.Count > 25 ? 25 : value.Payload.Count));

                        //  This is a good location to catch all exceptions occuring on media
                        //  data (writing to the file system) or other exceptions in the track
                        //  code.
                        try
                        {
                            DoNext(value);
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Frame buffer packet exception: " + ex.Message);
                            _logger.Error("Stack trace:" + ex.StackTrace);

                            //  Assumes that any exception in this frame buffer or track writer
                            //  code indicates that they are now in an unstable state.
                            //  Stop the reception of media packets because it is no longer
                            //  possible to store them in the track.
                            StopReceiving();
                        }
                    }
                }
            }
        }

        public void OnError(Exception error)
        {
            //  Just log the exception message but do not stop receiving. After an exception
            //  this observer should not receive any more OnNext() calls.
            //  The container object is going to call StopReceiving() when closing the container.
            _logger.Error("RTP session exception: " + error.Message);

            //StopReceiving();
        }

        public void OnCompleted()
        {
            //  Do nothing as the container object is responsible for calling StopReceiving()
            //  when closing the container.
        }

        #endregion

        protected abstract void DoNext(Packet packet);
    }
}
