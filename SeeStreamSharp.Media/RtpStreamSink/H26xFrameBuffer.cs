﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using System;
using System.Collections.Generic;
using System.Net;

namespace SeeStreamSharp.Media.RtpStreamSink
{
    internal abstract class H26xFrameBuffer : AbstractFrameBuffer
    {
        private int _fragmentSize;
        private int _fragmentSizeIndex = -1;

        public H26xFrameBuffer(ILog logger)
            : base(logger)
        {
        }

        /// <summary>
        /// Determine NAL unit size needed to be added to output.
        /// </summary>
        /// <param name="content">List of track writer byte segments already to be written to the
        /// container</param>
        /// <param name="index">The index in the frame buffer fragments list that is about to be
        /// added to the <paramref name="content"/> list</param>
        /// <returns>The number of bytes the STSZ frame size should be increased by</returns>
        public sealed override int ProcessFragmentConcatenation(IList<ArraySegment<byte>> content, int index)
        {
            Fragment fragment = _fragments[index];

            //  Don't add an invalid or excluded fragment.
            if (fragment.Payload == null)
                return -1;

            int bytesAdded = 0;
            //  Assume this fragment is self contained...
            bool addBeforeThisFragment = true;
            //  ...and its size is to be added to the output.
            int nalSize = fragment.Payload.Length;

            if ((fragment.Flags & FragmentFlags.FragmentedNal) == FragmentFlags.FragmentedNal)
            {
                if ((fragment.Flags & FragmentFlags.StartOfNalFragment) == FragmentFlags.StartOfNalFragment)
                {
                    //  Start calculating the NAL size.
                    _fragmentSize = 0;
                    //  Remember the index in the list where this start-of-NAL was
                    //  so the size can be inserted at its position.
                    _fragmentSizeIndex = content.Count;
                }

                _fragmentSize += fragment.Payload.Length;

                if ((fragment.Flags & FragmentFlags.EndOfNal) == FragmentFlags.EndOfNal)
                {
                    //  If fragment is end-of-NAL then the total size of the NAL is used...
                    nalSize = _fragmentSize;
                    //  ...and insert it at the start-of-NAL fragment remembered above.
                    addBeforeThisFragment = false;
                }
            }

            if ((fragment.Flags & FragmentFlags.EndOfNal) == FragmentFlags.EndOfNal)
            {
                byte[] fragmentSize = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(nalSize));

                if (addBeforeThisFragment)
                    //  Ok to just add to the end of content because the actual fragment
                    //  will be added to content by the caller when we return.
                    content.Add(new ArraySegment<byte>(fragmentSize));
                else
                {
                    //  TODO: This may also fail with exception because of out-of-sequence packets:
                    //  _fragmentSizeIndex == -1

                    //  Insert the 4 byte NAL size at the index of the remembered start-of-NAL.
                    content.Insert(_fragmentSizeIndex, new ArraySegment<byte>(fragmentSize));
                    _fragmentSizeIndex = -1;
                }

                bytesAdded = fragmentSize.Length;   //  Size of int (4).
            }

            return bytesAdded;
        }
    }
}
