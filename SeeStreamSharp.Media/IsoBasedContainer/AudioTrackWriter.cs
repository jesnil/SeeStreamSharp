﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

using Common.Logging;
using SeeStreamSharp.Media.RtpStreamSink;
using System.Collections.Generic;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    internal abstract class AudioTrackWriter : TrackWriter
    {
        private const uint SOUN = 0x736f756e;
        private const uint SMHD = 0x736d6864;
        private const int AUDIO_VOLUME = 0x0100;
        private const string description = "Audio Media Handler";

        private readonly uint _audioType;

        protected AudioTrackWriter(ILog logger, object container, AbstractFrameBuffer source,
            int clockRate, uint audioType)
            : base(logger, container, source, clockRate, AUDIO_VOLUME, SOUN, description)
        {
            _audioType = audioType;
        }

        /// <summary>
        /// Audio track type chunk content calculation.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="lastAddedIndex"></param>
        /// <returns>
        /// If less than Zero it is not yet a suitable chunk to write. If higher than Zero
        /// it is the number of entries in <paramref name="list"/> that is suitable to write
        /// as a chunk to the media container.
        /// </returns>
        /// <remarks>
        /// Using 10 second chunk content length is as suitable a selection as anything else.
        /// </remarks>
        protected sealed override int IsChunkReady(IList<AbstractFrameBuffer.Fragment> list, int lastAddedIndex)
        {
            long duration = list[lastAddedIndex].Timestamp;
            duration -= list[0].Timestamp;

            if (duration < 0)
                duration += uint.MaxValue;

            duration /= ClockRate;

            //  Write a chunk after receiving 10 seconds.
            return duration < 10 ? -1 : list.Count;
        }

        protected override sealed void MediaHeader()
        {
            _cw.AddBox(this, SMHD, SmhdAction);
        }

        private void SmhdAction()
        {
            _cw.Write(0, 0);

            _cw.Write(0); //  2 byte Balance + 2 byte reserved.
        }

        protected sealed override void AddSampleDescriptionBox()
        {
            _cw.AddBox(this, _audioType, AudioDescriptorAction);
        }

        protected abstract void AddDescriptorBox();

        private void AudioDescriptorAction()
        {
            //  General sample description fields:
            _cw.Write(0);                   //  Reserved
            _cw.WriteHalfWord(0);           //  Reserved
            _cw.WriteHalfWord(FIXED_SINGLE_DATA_REF_INDEX);

            //  Audio sample description fields:
            _cw.Write(0);                   //  Version + revision level
            _cw.Write(0);                   //  Vendor

            _cw.WriteHalfWord(2);           //  Channel count.
            _cw.WriteHalfWord(16);          //  Sample size.
            _cw.Write(0);                   //  Reserved
            _cw.Write(0);                   //  Timescale_of_media << 16

            AddDescriptorBox();
        }
    }
}
