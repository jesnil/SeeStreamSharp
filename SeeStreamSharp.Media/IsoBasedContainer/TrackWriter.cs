﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Media.RtpStreamSink;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    internal abstract class TrackWriter : IFrameBufferConsumer
    {
        #region Box types

        private const uint TRAK = 0x7472616b;
        private const uint TKHD = 0x746b6864;
        private const uint MDIA = 0x6d646961;
        private const uint MDHD = 0x6d646864;
        private const uint HDLR = 0x68646c72;
        private const uint MINF = 0x6d696e66;
        private const uint MHLR = 0x6d686c72;
        private const uint DINF = 0x64696e66;
        private const uint DREF = 0x64726566;
        private const uint URL_ = 0x75726c20;
        private const uint STBL = 0x7374626c;
        private const uint STSD = 0x73747364;
        private const uint STTS = 0x73747473;
        private const uint STSS = 0x73747373;
        private const uint STSC = 0x73747363;
        private const uint STSZ = 0x7374737a;
        private const uint STCO = 0x7374636f;
        private const uint CO64 = 0x636f3634;

        #endregion

        /// <summary>
        /// Default width
        /// </summary>
        protected readonly int _frameWidth = 800;
        /// <summary>
        /// Default height
        /// </summary>
        protected readonly int _frameHeight = 600;

        protected readonly ILog _logger;
        protected readonly int _loggingId;
        protected readonly ContainerWriter _cw;

        private readonly AbstractFrameBuffer _source;
        private readonly int _clockRate;
        private readonly int _volume;
        private readonly uint _hdlrSubtype;
        private readonly string _description;

        private readonly List<int> _frameSize = new List<int>(20 * 60);
        private readonly List<long> _chunkOffset = new List<long>(60);
        private List<int> _syncFrames;

        private struct ChunkItem
        {
            public readonly int ChunkNumber;
            public readonly int FrameCount;

            public ChunkItem(int chunkNumber, int frameCount)
            {
                ChunkNumber = chunkNumber;
                FrameCount = frameCount;
            }
        }

        private readonly List<ChunkItem> _chunkTable = new List<ChunkItem>(60);

        private struct DurationItem
        {
            public readonly int _duration;
            public int _count;

            public DurationItem(int duration)
            {
                _duration = duration;
                _count = 0;
            }
        }

        private readonly List<DurationItem> _durationTable = new List<DurationItem>(20 * 60);

        protected TrackWriter(ILog logger, object containerWriter, AbstractFrameBuffer source,
            int clockRate, int volume,
            uint hdlrSubtype, string description)
        {
            _logger = logger;
            _loggingId = RuntimeHelpers.GetHashCode(this);

            _cw = containerWriter as ContainerWriter;
            if (_cw == null)
                throw new ArgumentException("Non-compatible container", "containerWriter");
            _source = source;
            _clockRate = clockRate;
            _volume = volume;
            _hdlrSubtype = hdlrSubtype;
            _description = description;

            _cw.AddTrack(this);

            //  When logging is disabled it should not cause any garbage (heap
            //  allocations) at all in the key code path.
            FrameBufferUpdatedLogAction = new Action<FormatMessageHandler>(FrameBufferUpdatedLog);

            _logger.InfoFormat("{0} created", _loggingId);
        }

        #region No heap allocation logging

        private readonly Action<FormatMessageHandler> FrameBufferUpdatedLogAction;

        private IList<AbstractFrameBuffer.Fragment> _bufferList;
        private int _stopCount;

        private void FrameBufferUpdatedLog(FormatMessageHandler fmh)
        {
            AbstractFrameBuffer.Fragment toBeFirst = _bufferList[_stopCount - 1];
            string dump = toBeFirst.Payload == null ? "null" :
                BitConverter.ToString(toBeFirst.Payload, 0,
                                      toBeFirst.Payload.Length > 25 ? 25 : toBeFirst.Payload.Length);
            fmh("{0} stopCount={1} TimeStamp={2}{3}payload={4}",
                _loggingId, _stopCount, toBeFirst.Timestamp, Environment.NewLine, dump);
        }

        #endregion

        /// <summary>
        /// Make this track stop accepting more stream data in preparation for media container
        /// finalization.
        /// </summary>
        public void StopReceiving()
        {
            _source.StopReceiving();
        }

        #region Frame duration table

        private uint _previousTimestamp;

        private void AddFrameDuration(uint timestamp)
        {
            if (_frameSize.Count > 0)
            {
                long duration = timestamp;
                duration -= _previousTimestamp;

                //  Having a 90kHz RTP clock rate, a 32 bit time stamp wraps every 13 hour.
                if (duration < 0)
                    duration += uint.MaxValue;

                int count = _durationTable.Count;
                if (count == 0 || _durationTable[count - 1]._duration != duration)
                    _durationTable.Add(new DurationItem((int)duration));

                DurationItem last = _durationTable[_durationTable.Count - 1];
                last._count++;
                _durationTable[_durationTable.Count - 1] = last;
            }
            _previousTimestamp = timestamp;
        }

        public int ClockRate
        { get { return _clockRate; } }

        private int _duration;

        public int Duration
        {
            get
            {
                if (_duration == 0)
                {
                    //  Additional "assumed" last frame duration.
                    if (_durationTable.Count > 0)
                    {
                        DurationItem last = _durationTable[_durationTable.Count - 1];
                        last._count++;
                        _durationTable[_durationTable.Count - 1] = last;
                    }

                    long duration = 0;

                    for (int index = 0; index < _durationTable.Count; index++)
                    {
                        DurationItem di = _durationTable[index];
                        duration += di._duration * di._count;
                    }

                    _duration = (int)duration;
                }

                return _duration;
            }
        }

        #endregion

        #region Chunked media data

        private int _frameCountInLastChunk;

        /// <summary>
        /// Have the derived track type calculate if it the current "chunk" is ready to be
        /// written to the media container.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="lastAddedIndex"></param>
        /// <returns>
        /// If less than Zero it is not yet a suitable chunk to write. If higher than Zero
        /// it is the number of entries in <paramref name="list"/> that is suitable to write
        /// as a chunk to the media container.
        /// </returns>
        protected abstract int IsChunkReady(IList<AbstractFrameBuffer.Fragment> list, int lastAddedIndex);

        public int FrameBufferUpdated(IList<AbstractFrameBuffer.Fragment> bufferList, int addedIndex)
        {
            int stopCount = IsChunkReady(bufferList, addedIndex);
            if (stopCount > 0)
            {
                _bufferList = bufferList;
                _stopCount = stopCount;
                _logger.Trace(FrameBufferUpdatedLogAction);

                WriteChunk(bufferList, stopCount);
            }

            return stopCount;
        }

        public void FrameBufferFinalized(IList<AbstractFrameBuffer.Fragment> bufferList)
        {
            //  Flush received data to the file.
            if (bufferList.Count > 0)
            {
                int lastIndex = bufferList.Count - 1;

                while (lastIndex >= 0)
                {
                    AbstractFrameBuffer.Fragment cfLast = bufferList[lastIndex];

                    if ((cfLast.Flags & FragmentFlags.Marker) == FragmentFlags.Marker)
                        break;

                    lastIndex--;
                }

                if (lastIndex >= 0)
                    WriteChunk(bufferList, lastIndex + 1);
            }
        }

        private void WriteChunk(IList<AbstractFrameBuffer.Fragment> bufferList, int stopCount)
        {
            List<ArraySegment<byte>> content = new List<ArraySegment<byte>>(bufferList.Count);
            int currentFrameSize = 0;
            int chunkFrameCount = 0;
            bool currentFrameIsSync = false;

            for (int index = 0; index < stopCount; index++)
            {
                AbstractFrameBuffer.Fragment fragment = bufferList[index];

                int addSize = _source.ProcessFragmentConcatenation(content, index);
                if (addSize >= 0)
                {
                    content.Add(new ArraySegment<byte>(fragment.Payload));
                    currentFrameSize += fragment.Payload.Length;
                    currentFrameSize += addSize;
                }

                currentFrameIsSync |= (fragment.Flags & FragmentFlags.SyncFrame) == FragmentFlags.SyncFrame;

                if ((fragment.Flags & FragmentFlags.Marker) == FragmentFlags.Marker)
                {
                    AddFrameDuration(fragment.Timestamp);

                    _frameSize.Add(currentFrameSize);
                    chunkFrameCount++;

                    if (currentFrameIsSync)
                    {
                        if (_syncFrames == null)
                            _syncFrames = new List<int>(60);
                        _syncFrames.Add(_frameSize.Count);
                    }
                    currentFrameIsSync = false;

                    currentFrameSize = 0;
                }
            }

            long chunkOffset = _cw.AddMdatContent(this, content);
            if (chunkOffset > 0)
            {
                Debug.WriteLine("chunkOffset = " + chunkOffset);
                _chunkOffset.Add(chunkOffset);
                if (chunkFrameCount != _frameCountInLastChunk)
                {
                    Debug.WriteLine("_chunkOffset.Count = " + _chunkOffset.Count + ", chunkFrameCount = " + chunkFrameCount);
                    _chunkTable.Add(new ChunkItem(_chunkOffset.Count, chunkFrameCount));
                    _frameCountInLastChunk = chunkFrameCount;
                }
            }
        }

        #endregion

        #region Media track box writing

        protected int _trackId;

        public void AddTrack(ContainerWriter cw, int trackId)
        {
            if (!ReferenceEquals(cw, _cw))
                throw new ArgumentException("Wrong container", "cw");

            _trackId = trackId;
            _cw.AddBox(this, TRAK, TrakAction); //  Mandatory box.
        }

        private const int TRACK_ENABLED_FLAG = 1;
        private const int TRACK_IN_MOVIE_FALG = 2;  //  Used in the presentation.
        private const int TRACK_IN_PREVIEW_FLAG = 4;

        private void TrakAction()
        {
            _cw.AddBox(this, TKHD, TkhdAction); //  Mandatory box.
            _cw.AddBox(this, MDIA, MdiaAction); //  Mandatory box.
        }

        private void TkhdAction()
        {
            _cw.Write(0, TRACK_ENABLED_FLAG | TRACK_IN_MOVIE_FALG | TRACK_IN_PREVIEW_FLAG);

            //  Version 0 uses 32 bits for the following 5 items.
            _cw.Write(_cw._isoCreationTime);
            _cw.Write(_cw._isoCreationTime);    //  Modification time.
            _cw.Write(_trackId);
            _cw.Write(0);                       //  Reserved
            _cw.Write(Duration);

            _cw.Write(0);                   //  Reserved
            _cw.Write(0);                   //  Reserved
            _cw.Write(0);                   //  Layer + alternate group.
            _cw.WriteHalfWord(_volume);     //  Volume (if this is an audio track)
            _cw.WriteHalfWord(0);           //  Reserved
            _cw.Write(0x00010000);          //  Start of transformation matrix.
            _cw.Write(0);
            _cw.Write(0);
            _cw.Write(0);
            _cw.Write(0x00010000);
            _cw.Write(0);
            _cw.Write(0);
            _cw.Write(0);
            _cw.Write(0x40000000);          //  End of transformation matrix.
            _cw.Write(_frameWidth << 16);   //  Track width in fixed-point 16.16 value.
            _cw.Write(_frameHeight << 16);  //  Track height in fixed-point 16.16 value.
        }

        private void MdiaAction()
        {
            _cw.AddBox(this, MDHD, MdhdAction); //  Mandatory box.
            _cw.AddBox(this, HDLR, HdlrAction); //  Mandatory box.
            _cw.AddBox(this, MINF, MinfAction); //  Mandatory box.
        }

        private void MdhdAction()
        {
            _cw.Write(0, 0);

            //  Version 0 uses 32 bits for the following 4 items.
            _cw.Write(_cw._isoCreationTime);
            _cw.Write(_cw._isoCreationTime);  //  Modification time.
            _cw.Write(_clockRate);
            _cw.Write(Duration);          //  Duration.

            _cw.Write(0);                 //  Language+Quality.
        }

        private void HdlrAction()
        {
            _cw.Write(0, 0);

            _cw.Write(MHLR);              //  Component type.
            _cw.Write(_hdlrSubtype);      //  Component subtype.
            _cw.Write(0);                 //  Component manufacturer.
            _cw.Write(0);                 //  Component flags.
            _cw.Write(0);                 //  Component flags mask.
            _cw.Write(_description);
        }

        /// <summary>
        /// Write the derived media track type Media Header box.
        /// </summary>
        protected abstract void MediaHeader();

        private void MinfAction()
        {
            MediaHeader();

            _cw.AddBox(this, DINF, DinfAction); //  Mandatory box.
            _cw.AddBox(this, STBL, StblAction); //  Mandatory box.
        }

        private void DinfAction()
        {
            _cw.AddBox(this, DREF, DrefAction); //  Mandatory box.
        }

        private void DrefAction()
        {
            _cw.Write(0, 0);

            _cw.Write(1);                       //  Number of entries.
            _cw.AddBox(this, URL_, UrlAction);  //  Mandatory box.
        }

        private const int MEDIA_DATA_IN_THIS_FILE_FLAG = 1;

        /// <summary>
        /// Write empty box to signal media data contained in this file.
        /// </summary>
        private void UrlAction()
        {
            _cw.Write(0, MEDIA_DATA_IN_THIS_FILE_FLAG);
        }

        private void StblAction()
        {
            _cw.AddBox(this, STSD, StsdAction); //  Mandatory box.
            _cw.AddBox(this, STTS, SttsAction); //  Mandatory box.
            _cw.AddBox(this, STSC, StscAction); //  Mandatory box.
            _cw.AddBox(this, STSZ, StszAction); //  Mandatory box.

            //  A chunk offset box is mandatory but it can be either an STCO or CO64.
            _cw.AddBox(this, CO64, Co64Action);

            //  STSS is typicaly only for video streams.
            if (_syncFrames != null)
                _cw.AddBox(this, STSS, StssAction);
        }

        protected const int FIXED_SINGLE_DATA_REF_INDEX = 1;

        /// <summary>
        /// Write the derived media sample description box.
        /// </summary>
        protected abstract void AddSampleDescriptionBox();

        /// <summary>
        /// Writes the Sample Description Box 'STSD' in the track.
        /// </summary>
        /// <remarks>
        /// Hard coded to a single sample description per track.
        /// </remarks>
        private void StsdAction()
        {
            _cw.Write(0, 0);

            _cw.Write(1);     //  Entry count.
            AddSampleDescriptionBox();
        }

        private void SttsAction()
        {
            _cw.Write(0, 0);

            _cw.Write(_durationTable.Count);
            foreach (DurationItem di in _durationTable)
            {
                _cw.Write(di._count);
                _cw.Write(di._duration);
            }
        }

        private void StscAction()
        {
            _cw.Write(0, 0);

            _cw.Write(_chunkTable.Count);
            for (int index = 0; index < _chunkTable.Count; index++)
            {
                ChunkItem item = _chunkTable[index];
                _cw.Write(item.ChunkNumber);
                _cw.Write(item.FrameCount);
                _cw.Write(FIXED_SINGLE_DATA_REF_INDEX);
            }
        }

        private void StszAction()
        {
            _cw.Write(0, 0);

            _cw.Write(0);                  //  Indicates a multiple-entry table.
            _cw.Write(_frameSize.Count);
            for (int index = 0; index < _frameSize.Count; index++)
                _cw.Write(_frameSize[index]);
        }

        private void Co64Action()
        {
            _cw.Write(0, 0);

            _cw.Write(_chunkOffset.Count);
            for (int index = 0; index < _chunkOffset.Count; index++)
                _cw.Write(_chunkOffset[index]);
        }

        private void StssAction()
        {
            _cw.Write(0, 0);

            _cw.Write(_syncFrames.Count);   //  Number of entries.
            foreach (int item in _syncFrames)
                _cw.Write(item);          //  Sync frame number.
        }

        #endregion
    }
}
