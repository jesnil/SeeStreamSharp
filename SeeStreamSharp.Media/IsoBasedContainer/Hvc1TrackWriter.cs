﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Media.RtpStreamSink;
using SeeStreamSharp.Media.SessionDescriptionSetup;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    /// <summary>
    /// HVC1 track writer.
    /// </summary>
    class Hvc1TrackWriter : VideoTrackWriter, IH264Consumer
    {
        private const uint HVC1 = 0x68766331;
        private const uint HVCC = 0x68766343;   //  "hvcC"
        //private const uint BTRT = 0x68766331;
        private const int ARRAY_COMPLETENESS_BIT = 0x80;
        private const byte LENGTH_SIZE_MINUS_ONE = 3;   //  Hard coded to use 4 byte NALU length.

        //  First HVCC byte
        private static readonly BitVector32.Section s_general_profile_idc = BitVector32.CreateSection((1 << 4) - 1);
        private static readonly BitVector32.Section s_general_tier_flag = BitVector32.CreateSection(1, s_general_profile_idc);
        private static readonly BitVector32.Section s_general_profile_space = BitVector32.CreateSection((1 << 1) - 1, s_general_tier_flag);

        private readonly BitVector32 _firstByte = new BitVector32();
        private readonly H265SinkFactory _sink;

        private bool _vpsObserved, _spsObserved, _ppsObserved;

        public Hvc1TrackWriter(object container, int clockRate,
            H265SinkFactory sink, AbstractFrameBuffer source)
            : base(LogManager.GetLogger<Hvc1TrackWriter>(), container, source, clockRate, HVC1)
        {
            _sink = sink;

            _firstByte[s_general_profile_space] = _sink._ps;
            _firstByte[s_general_tier_flag] = _sink._tf;
            _firstByte[s_general_profile_idc] = _sink._pi;
        }

        protected override void AddDescriptorBox()
        {
            _cw.AddBox(this, HVCC, HvccAction);
            //_cw.AddBox(this, BTRT, BtrtAction);   //  Optional atom...
        }

        /*
            aligned(8) class HEVCDecoderConfigurationRecord {
               unsigned int(8) configurationVersion = 1;
               unsigned int(2) general_profile_space;
               unsigned int(1) general_tier_flag;
               unsigned int(5) general_profile_idc;
               unsigned int(32) general_profile_compatibility_flags;
               unsigned int(48) general_constraint_indicator_flags;
               unsigned int(8) general_level_idc;
               bit(4) reserved = ‘1111’b;
               unsigned int(12) min_spatial_segmentation_idc;
               bit(6) reserved = ‘111111’b;
               unsigned int(2) parallelismType;
               bit(6) reserved = ‘111111’b;
               unsigned int(2) chroma_format_idc;
               bit(5) reserved = ‘11111’b;
               unsigned int(3) bit_depth_luma_minus8;
               bit(5) reserved = ‘11111’b;
               unsigned int(3) bit_depth_chroma_minus8;
               bit(16) avgFrameRate;
               bit(2) constantFrameRate;
               bit(3) numTemporalLayers;
               bit(1) temporalIdNested;
               unsigned int(2) lengthSizeMinusOne;
               unsigned int(8) numOfArrays;
               for (j=0; j < numOfArrays; j++) {
                  bit(1) array_completeness;
                  unsigned int(1) reserved = 0;
                  unsigned int(6) NAL_unit_type;
                  unsigned int(16) numNalus;
                  for (i=0; i< numNalus; i++) {
                     unsigned int(16) nalUnitLength;
                     bit(8*nalUnitLength) nalUnit;
                  }
               }
            }
         */
        private void HvccAction()
        {
            _cw.Write((byte)1);                 //  Configuration version.

            _cw.Write((byte)_firstByte.Data);   //  Start of general configuration.
            _cw.Write(_sink._pci, 4);
            _cw.Write(_sink._ic, 6);
            _cw.Write((byte)_sink._levelId);

            _cw.WriteHalfWord(0xf000 | _sink._min_spatial_segmentation_idc);
            _cw.Write((byte)0xfc);              //  parallelismType. Not available in SDP.
            _cw.Write((byte)0xfc);              //  chroma_format_idc. Not available in SDP.
            _cw.Write((byte)0xf8);              //  bit_depth_luma_minus8. Not available in SDP.
            _cw.Write((byte)0xf8);              //  bit_depth_chroma_minus8. Not available in SDP.
            _cw.WriteHalfWord(0);               //  avgFramerate. Not available in SDP.
            _cw.Write(LENGTH_SIZE_MINUS_ONE);   //  constantFrameRate, numTemporalLayer,
                                                //  and temporalIdNested not available in SDP.
                                                //  Only lengthSizeMinusOne available.

            int iNumArrays = 0;
            if (_sink._vps != null && _sink._vps.Count > 0)
                iNumArrays++;
            if (_sink._sps != null && _sink._sps.Count > 0)
                iNumArrays++;
            if (_sink._pps != null && _sink._pps.Count > 0)
                iNumArrays++;

            _cw.Write((byte)iNumArrays);         //  iNumArrays (Byte 23 in this atom!)
            if (_sink._vps != null && _sink._vps.Count > 0)
                AddParmeterSet(_sink._vps, _vpsObserved);
            if (_sink._sps != null && _sink._sps.Count > 0)
                AddParmeterSet(_sink._sps, _spsObserved);
            if (_sink._pps != null && _sink._pps.Count > 0)
                AddParmeterSet(_sink._pps, _ppsObserved);
        }

        private void AddParmeterSet(List<byte[]> parameter, bool observed)
        {
            //  Assume all byte arrays in the list are same NAL type so only get it from the first.
            BitVector32 nal = new BitVector32(IPAddress.NetworkToHostOrder(BitConverter.ToInt16(parameter[0], 0)));

            //  First byte is nalType |= array_completeness. A set array_completeness indicates
            //  that all NAL units of this type is in this list. Not set indicates that additional
            //  NAL units of the indicated type may be in the stream
            int nalCompleteness = nal[H265FrameBuffer.s_nalType] |= observed ? 0 : ARRAY_COMPLETENESS_BIT;
            _cw.Write((byte)nalCompleteness);

            //  Following word is number of nalus.
            _cw.WriteHalfWord(parameter.Count);
            for (int i = 0; i < parameter.Count; i++)
            {
                _cw.WriteHalfWord(parameter[i].Length);
                _cw.Write(parameter[i], parameter[i].Length);
            }
        }

        /// <summary>
        /// Signal that a parameter set NAL has been included in the RTP packet stream.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public void AddToParameterSets(byte[] buffer, int offset, int count)
        {
            BitVector32 nalUnitType = new BitVector32(IPAddress.NetworkToHostOrder(BitConverter.ToInt16(buffer, 0)));

            switch (nalUnitType[H265FrameBuffer.s_nalType])
            {
                case H265FrameBuffer.NAL_UNIT_VPS:
                    _vpsObserved = true;
                    break;
                case H265FrameBuffer.NAL_UNIT_SPS:
                    _spsObserved = true;
                    break;
                case H265FrameBuffer.NAL_UNIT_PPS:
                    _ppsObserved = true;
                    break;
            }
        }
    }
}


