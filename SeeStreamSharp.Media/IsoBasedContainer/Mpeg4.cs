﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using System.Collections.Generic;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    internal static class Mpeg4
    {
        private const byte ES_DESCR_TAG = 0x03;

        private const byte DECODER_CONFIG_DESCR_TAG = 0x04;
        public const byte VISUAL_ISO_14496_2 = 0x20;
        public const byte AUDIO_ISO_14496_3 = 0x40;
        public const byte VISUAL_STREAM = 0x04;
        public const byte AUDIO_STREAM = 0x05;

        private const byte DEC_SPECIFIC_INFO_TAG = 0x05;

        private const byte SL_CONFIG_DESCR_TAG = 0x06;
        private const byte RESERVED_FOR_USE_IN_MP4_FILES = 2;

        public static byte[] EsDescriptor(int trackId, byte objectTypeIndicator, byte streamType, byte[] config)
        {
            List<byte> descriptor = new List<byte>(20 + config.Length);

            //  Insert SLConfigDescriptor
            descriptor.Insert(0, RESERVED_FOR_USE_IN_MP4_FILES);    //  Predefined setting in ISO 14496-1.
            //  Insert the SLConfigDescrTag
            descriptor.InsertTagAndLength(SL_CONFIG_DESCR_TAG);

            //  Insert config array.
            descriptor.InsertRange(0, config);
            //  Insert the Decoder specific descriptor type tag.
            descriptor.InsertLength(config.Length);
            descriptor.Insert(0, DEC_SPECIFIC_INFO_TAG);

            //  Insert DecoderConfigDescriptor.
            descriptor.Insert(0, objectTypeIndicator);          //  objectTypeIndication
            descriptor.Insert(1, (byte)(streamType << 2 | 1));  //  upstream '0' | reserved '1'
            //  24 bit decoding buffer size, network byte order.
            descriptor.Insert(2, 0x10);                 //  1 MB (0x10 0x00 0x00)
            descriptor.Insert(3, 0);
            descriptor.Insert(4, 0);
            //  32 bit maxBitrate, network byte order.
            descriptor.Insert(5, 0x7f);
            descriptor.Insert(6, 0xff);
            descriptor.Insert(7, 0xff);
            descriptor.Insert(8, 0xff);
            //  32 bit avgBitrate, network byte order.
            descriptor.Insert(9, 0);                    //  For streams with variable
            descriptor.Insert(10, 0);                   //  bitrate this value shall
            descriptor.Insert(11, 0);                   //  be set to zero.
            descriptor.Insert(12, 0);
            //  Insert the Decoder specific descriptor type tag.
            descriptor.InsertTagAndLength(DECODER_CONFIG_DESCR_TAG);

            //  Insert ES_Descriptor.
            //  16 bit ES_ID, network byte order.
            descriptor.Insert(0, (byte)(trackId >> 8));
            descriptor.Insert(1, (byte)(trackId));
            //  Byte, flags and priority.
            descriptor.Insert(2, 0x10);         //  Priority 16 of 31.
            //  Insert the Decoder specific descriptor type tag.
            descriptor.InsertTagAndLength(ES_DESCR_TAG);

            return descriptor.ToArray();
        }

        private static void InsertTagAndLength(this List<byte> list, byte tag)
        {
            list.InsertLength(list.Count);

            list.Insert(0, tag);
        }

        private static void InsertLength(this List<byte> list, int length)
        {
            byte value = (byte)(length & 0x7F);
            list.Insert(0, value);

            length = length >> 7;
            while (length > 0)
            {
                value = (byte)(length | 0x80);
                list.Insert(0, value);

                length = length >> 7;
            }
        }
    }
}
