﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using Common.Logging;
using SeeStreamSharp.Media.RtpStreamSink;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    internal sealed class Mp4aTrackWriter : AudioTrackWriter
    {
        private const uint MP4A = 0x6d703461;
        private const uint ESDS = 0x65736473;

        //  To be able to separate a worker thread updating track data while at the same
        //  time another thread is attempting to stop the recording.
        private readonly object _sinkWriteChunkLock = new object();

        private readonly byte[] _config;

        public Mp4aTrackWriter(object container, int clockRate,
            byte[] config, AbstractFrameBuffer source)
            : base(LogManager.GetLogger<Mp4aTrackWriter>(), container, source, clockRate, MP4A)
        {
            _config = config;
        }

        protected sealed override void AddDescriptorBox()
        {
            _cw.AddBox(this, ESDS, EsdsAction);
        }

        private void EsdsAction()
        {
            _cw.Write(0, 0);

            byte[] elementaryStreamDescriptor =
                Mpeg4.EsDescriptor(_trackId, Mpeg4.AUDIO_ISO_14496_3, Mpeg4.AUDIO_STREAM, _config);
            _cw.Write(elementaryStreamDescriptor, elementaryStreamDescriptor.Length);
        }
    }
}
