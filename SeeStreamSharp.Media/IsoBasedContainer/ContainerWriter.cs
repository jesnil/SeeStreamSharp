﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.Media.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace SeeStreamSharp.Media.IsoBasedContainer
{
    /// <summary>
    /// A media container writer creating an ISO 14496-12:2005 specification file.
    /// </summary>
    /// <remarks>
    /// The implementation is augmented to setting the compatible brand in the file to the
    /// ISO 14496-14:2003 MP4 file specification and also uses the ".MP4" file extension.
    /// </remarks>
    internal sealed class ContainerWriter : IContainerInfo, IDisposable
    {
        private const uint FTYP = 0x66747970;
        private const uint ISOM = 0x69736f6d;
        private const uint MP42 = 0x6d703432;
        private const uint MDAT = 0x6d646174;
        private const uint MOOV = 0x6d6f6f76;
        private const uint MVHD = 0x6d766864;

        private const int ISO_MOVIE_TIME_SCALE = 600;  //  Ticks per second.

        private static readonly DateTime ISO_EPOCH = new DateTime(1904, 1, 1, 0, 0, 0, DateTimeKind.Local);

        private readonly List<TrackWriter> _tracks = new List<TrackWriter>(2);
        internal readonly uint _isoCreationTime;    //  32 bits are sufficient until approximately year 2040.
        private readonly BinaryWriter _bw;

        private long _mdatPosition;
        private TrackWriter _currentTrack;
        private uint _longestTrackDuration;

        public string FileName { get; private set; }

        /// <summary>
        /// Create a ISO based media container.
        /// </summary>
        /// <param name="path"></param>
        /// <remarks>
        /// This has to be public as this constructor is called using reflection.
        /// </remarks>
        public ContainerWriter(string path)
        {
            TimeSpan ts = DateTime.Now - ISO_EPOCH;
            _isoCreationTime = (uint)ts.TotalSeconds;

            FileName = Path.ChangeExtension(path, Settings.Default.IsoBasedContainerFileExtension);
            _bw = new BinaryWriter(File.Open(FileName, FileMode.Create, FileAccess.Write, FileShare.Read));

            //  Start by writing the file header box.
            AddBox(null, FTYP, FtypAction); //  Mandatory box.

            //  Add an initially empty MDAT box.
            _mdatPosition = _bw.BaseStream.Position;
            Write(0);       //  32 bit box size placeholder.
            Write(MDAT);
            Write((long)0); //  Size placeholder for a possible 64 bit MDAT box length.
        }

        public void AddTrack(TrackWriter track)
        {
            _tracks.Add(track);
        }

        public long AddMdatContent(TrackWriter track, IList<ArraySegment<byte>> buffers, int startIndex = 0, int endIndex = int.MaxValue)
        {
            if (!_tracks.Contains(track))
                throw new ArgumentException("Not a track in this container", "track");

            if (buffers.Count < endIndex)
                endIndex = buffers.Count;

            long startPosiotion = 0;

            if (_mdatPosition != 0)
            {
                //  Use a lock to do a continues write of the list of byte array segments 
                lock (_bw)
                {
                    if (_mdatPosition != 0)
                    {
                        startPosiotion = _bw.BaseStream.Position;
                        while (startIndex < endIndex)
                        {
                            ArraySegment<byte> buffer = buffers[startIndex];
                            _bw.BaseStream.Write(buffer.Array, buffer.Offset, buffer.Count);
                            startIndex++;
                        }
                    }
                }
            }

            return startPosiotion;
        }

        public void Dispose()
        {
            //  Stop the track writers from accepting more packets and flush the media data
            //  still in memory to the MDAT box.
            foreach (TrackWriter track in _tracks)
                track.StopReceiving();

            //  FinalizeMdatBox() will use the lock to reset _mdatPosition to zero.
            //  After this locking is no longer required.
            FinalizeMdatBox();

            //_longestTrackDuration = 0;

            foreach (TrackWriter track in _tracks)
            {
                long trackDuration = track.Duration;
                trackDuration *= ISO_MOVIE_TIME_SCALE;
                trackDuration /= track.ClockRate;

                if (trackDuration > _longestTrackDuration)
                    _longestTrackDuration = (uint)trackDuration;
            }

            //  Start writing the movie box. It will also add all the tracks.
            AddBox(null, MOOV, MoovAction); //  Mandatory box.

            //  Close the media container.
            _bw.Dispose();
        }

        private void FinalizeMdatBox()
        {
            long size;

            lock (_bw)
            {
                size = _bw.BaseStream.Position - _mdatPosition;

                _bw.BaseStream.Seek(_mdatPosition, SeekOrigin.Begin);
                _mdatPosition = 0;
            }

            //  The stream will now write at the MDAT size position.
            if (size < uint.MaxValue)
                Write((uint)size);
            else
            {
                Write(1);         //  64 bit size marker
                Write(MDAT);
                Write(size);
            }

            _bw.BaseStream.Seek(0, SeekOrigin.End);
        }

        private void FtypAction()
        {
            Write(ISOM);  //  Major brand.
            Write(0);     //  Version
            //  Add an ISO 14496-14:2003 MP4 file specification compatible brand.
            Write(MP42);
        }

        private void MoovAction()
        {
            AddBox(null, MVHD, MvhdAction); //  Mandatory box.

            //  MDAT writing is disabled by resetting _mdatPosition to zero.
            //  No locking is required.
            for (int index = 0; index < _tracks.Count; index++)
            {
                _currentTrack = _tracks[index];
                //  The track id used can not be zero.
                _currentTrack.AddTrack(this, index + 1);
            }
            _currentTrack = null;
        }

        private void MvhdAction()
        {
            Write(0, 0);

            //  Version 0 uses 32 bits for the following 4 items.
            Write(_isoCreationTime);
            Write(_isoCreationTime);    //  Modification time.
            Write(ISO_MOVIE_TIME_SCALE);
            Write(_longestTrackDuration);

            Write(0x00010000);      //  Rate 1.0 is normal forward playback.
            WriteHalfWord(0x0100);  //  Volume 1.0 is full volume.
            WriteHalfWord(0);       //  Reserved
            Write(0);               //  Reserved1.
            Write(0);               //  Reserved2.
            Write(0x00010000);      //  Start of transformation matrix.
            Write(0);
            Write(0);
            Write(0);
            Write(0x00010000);
            Write(0);
            Write(0);
            Write(0);
            Write(0x40000000);      //  End of transformation matrix.
            Write(0);               //  Various time fields 1.
            Write(0);               //  Various time fields 2.
            Write(0);               //  Various time fields 3.
            Write(0);               //  Various time fields 4.
            Write(0);               //  Various time fields 5.
            Write(0);               //  Various time fields 6.
            Write(_tracks.Count + 1);// Next available track ID in this container.
        }

        public void AddBox(TrackWriter writer, uint boxId, Action contentWriter)
        {
            if (!ReferenceEquals(writer, _currentTrack))
                throw new ArgumentException("Not currently allowed", "writer");
            //  _mdatPosition is initially 0 when FTYP is written and then set.
            if (_mdatPosition != 0)
                throw new InvalidOperationException("Boxes can not be added at this time");

            long boxLengthPosition = _bw.BaseStream.Position;
            Write(0);     //  Size placeholder
            Write(boxId);

            contentWriter();

            long size = _bw.BaseStream.Position - boxLengthPosition;
            _bw.BaseStream.Seek(boxLengthPosition, SeekOrigin.Begin);
            Write((uint)size);
            _bw.BaseStream.Seek(0, SeekOrigin.End);
        }

        public void Write(byte version, int flags)
        {
            Write(version);
            Write((byte)(flags >> 16));
            Write((byte)(flags >> 8));
            Write((byte)(flags));
        }

        public void Write(byte value)
        {
            _bw.Write(value);
        }

        public void Write(byte[] data, int count)
        {
            _bw.Write(data, 0, count);
        }

        public void WriteHalfWord(int value)
        {
            _bw.Write(IPAddress.HostToNetworkOrder((short)value));
        }

        public void Write(int value)
        {
            _bw.Write(IPAddress.HostToNetworkOrder(value));
        }

        public void Write(uint value)
        {
            Write((int)value);
        }

        public void Write(long value)
        {
            _bw.Write(IPAddress.HostToNetworkOrder(value));
        }

        public void Write(string value, bool oneByteLengthIndicator = true)
        {
            int l = value.Length;
            if (l > 255)
                l = 255;

            if (oneByteLengthIndicator)
                _bw.Write((byte)l);

            byte[] data = Encoding.UTF8.GetBytes(value);
            _bw.Write(data, 0, l);
        }
    }
}
