﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.Media.IsoBasedContainer;
using SeeStreamSharp.Media.RtpStreamSink;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    /// <summary>
    /// HEVC (H.265) SDP media description to stream sink factory converter.
    /// </summary>
    /// <remarks>
    /// Processes SDP media attributes as documented in RFC 7798 RTP Payload Format for
    /// High Efficiency Video Coding (HEVC) (https://www.rfc-editor.org/rfc/rfc7798).
    /// </remarks>
    [MediaFormatName("H265")]
    internal class H265SinkFactory : H26xSinkFactory, IStreamSinkFactory
    {
        private const string profileCompatibilityIndicator = "profile-compatibility-indicator=";
        private const int profileCompatibilityIndicatorByteCount = 4;
        private const string profileSpace = "profile-space=";
        private const string profileId = "profile-id=";
        private const string tierFlag = "tier-flag=";
        private const string levelId = "level-id=";
        private const string interopConstraints = "interop-constraints=";
        private const int interopConstraintsByteCount = 6;
        private const string spropSpatialSegmentationIdc = "sprop-spatial-segmentation-idc=";
        private const string spropMaxDonDiff = "sprop-max-don-diff=";
        private const string spropVps = "sprop-vps=";
        private const string spropSps = "sprop-sps=";
        private const string spropPps = "sprop-pps=";

        internal readonly int _ps;
        internal readonly int _tf;
        internal readonly int _pi;

        internal readonly byte[] _pci;
        internal readonly byte[] _ic;
        internal readonly int _levelId;  //  Value 0-255
        internal readonly int _min_spatial_segmentation_idc;
        internal readonly int _maxDonDiff;
        internal readonly List<byte[]> _vps;
        internal readonly List<byte[]> _sps;
        internal readonly List<byte[]> _pps;

        public H265SinkFactory(RtpMapAttribute rtpMap) : base(rtpMap)
        {
            byte[] min_spatial_segmentation_idc = null;

            try
            {
                //  Because the member fields the values are stored in are read-only a chained
                //  Where() is not permitted.
                foreach (string item in GetFormatParameters())
                {
                    item.FmtpParser(profileSpace, ref _ps, ps => {
                        if (ps > 3 || ps < 0)
                            throw new ArgumentException("Invalid 'profile-space' value", "rtpMap");
                    });

                    item.FmtpParser(tierFlag, ref _tf, tf => {
                        if (tf > 1 || tf < 0)
                            throw new ArgumentException("Invalid 'tier-flag' value", "rtpMap");
                    });

                    item.FmtpParser(profileId, ref _pi, pi => {
                        if (pi > 31 || pi < 0)
                            throw new ArgumentException("Invalid 'profile-id' value", "rtpMap");
                    });

                    item.FmtpParser(profileCompatibilityIndicator, ref _pci, profileCompatibilityIndicatorByteCount);

                    item.FmtpParser(interopConstraints, ref _ic, interopConstraintsByteCount);

                    item.FmtpParser(levelId, ref _levelId, levelId => {
                        if (levelId > 255 || levelId < 0)
                            throw new ArgumentException("Invalid 'level-id' value", "rtpMap");
                    });

                    item.FmtpParser(spropSpatialSegmentationIdc, ref min_spatial_segmentation_idc, FormatParser.NO_EXPECTED_LENGTH);

                    item.FmtpParser(spropMaxDonDiff, ref _maxDonDiff, maxDonDiff => {
                        if (maxDonDiff > 32767 || maxDonDiff < 0)
                            throw new ArgumentException("Invalid 'sprop-max-don-diff' value", "rtpMap");
                    });

                    item.FmtpParser(spropVps, ref _vps, false);

                    item.FmtpParser(spropSps, ref _sps, false);

                    item.FmtpParser(spropPps, ref _pps, false);
                }
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException("No associated Fmtp attribute", "rtpMap");
            }

            //if (_ps == -1 || _tf == -1 || _pi == -1)
            //    throw new ArgumentException("Mandatory format parameters missing", "rtpMap");

            if (_pci == null)
                _pci = new byte[profileCompatibilityIndicatorByteCount];
            if (_ic == null)
                _ic = new byte[interopConstraintsByteCount];

            if (min_spatial_segmentation_idc != null)
            {
                _min_spatial_segmentation_idc = min_spatial_segmentation_idc.Length == 1 ?
                    min_spatial_segmentation_idc[0] :
                    min_spatial_segmentation_idc[0] << 8 | min_spatial_segmentation_idc[1];
            }
        }

        public void ConnectStreamSink(object container, Session session)
        {
            H265FrameBuffer frameBuffer = new H265FrameBuffer(_maxDonDiff);
            Hvc1TrackWriter track = new Hvc1TrackWriter(container, _rtpMap.ClockRate,
                this, frameBuffer);
            frameBuffer.Connect(track, session);
        }

        public Type GetContainerType()
        {
            return typeof(ContainerWriter);
        }
    }
}
