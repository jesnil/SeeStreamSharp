﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    internal class H26xSinkFactory
    {
        //  Default clock rate according to both RFC 6184 and RFC 7798 is 90000.
        protected readonly RtpMapAttribute _rtpMap;

        internal H26xSinkFactory(RtpMapAttribute rtpMap)
        {
            if (rtpMap == null)
                throw new ArgumentNullException("rtpMap");

            _rtpMap = rtpMap;
        }

        protected IEnumerable<string> GetFormatParameters()
        {
            //  There should only be one FmtpAttribute item that has the same PayloadType
            //  as the RtpMapAttribute.
            return _rtpMap.MediaParent.OfType<FmtpAttribute>()
                .Where(o => o.PayloadType == _rtpMap.PayloadType)
                .Single()   //  Exacly one single entry or throw InvalidOperationException.
                .FormatParameters.Split(';');
        }
    }
}
