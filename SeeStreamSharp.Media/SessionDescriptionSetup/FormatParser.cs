﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.Media.RtpStreamSink;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    internal static class FormatParser
    {
        internal const int NO_EXPECTED_LENGTH = -1;

        private static bool FmtpParser<T>(string formatParameter,
            string name, ref T field,
            Func<string, T> parse, Action<T> postProcessor)
        {
            int index = formatParameter.IndexOf(name, StringComparison.Ordinal);
            if (index != -1)
            {
                field = parse(formatParameter.Substring(index + name.Length));
                postProcessor?.Invoke(field);
                return false;
            }
            return true;
        }

        internal static void FmtpParser(this string item, string name, ref int field, Action<int> postProcessor)
        {
            FmtpParser(item, name, ref field, input => int.Parse(input), postProcessor);
        }

        internal static void FmtpParser(this string item, string name, ref byte[] field, int expectedLength)
        {
            FmtpParser(item, name, ref field, input => {
                SoapHexBinary shb = SoapHexBinary.Parse(input);
                return shb != null && (shb.Value.Length == expectedLength || expectedLength == -1) ? shb.Value : null;
            }, null);
        }

        internal static void FmtpParser(this string item, string name, ref List<byte[]> field, bool h264SpsOrPpsFiltering)
        {
            FmtpParser(item, name, ref field, input => {
                string[] props = input.Split(',');
                List<byte[]> parameterSets = new List<byte[]>();

                foreach (string prop in props)
                {
                    //  May throw an exception if prop is malformed.
                    byte[] data = Convert.FromBase64String(prop);
                    if (h264SpsOrPpsFiltering)
                    {
                        BitVector32 nal = new BitVector32(data[0]);
                        int nalType = nal[H264FrameBuffer.s_nalType];
                        if (nalType == H264FrameBuffer.NAL_SPS || nalType == H264FrameBuffer.NAL_PPS)
                            parameterSets.Add(data);
                    }
                    else
                        parameterSets.Add(data);
                }

                return parameterSets;
            }, null);
        }
    }
}
