﻿/*
    Copyright 2015 Jesper Nilsson

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
using SeeStreamSharp.Media.IsoBasedContainer;
using SeeStreamSharp.Media.RtpStreamSink;
using SeeStreamSharp.RTP;
using SeeStreamSharp.SDP;
using System;
using System.Collections.Generic;

namespace SeeStreamSharp.Media.SessionDescriptionSetup
{
    /// <summary>
    /// H.264 SDP media description to stream sink factory converter.
    /// </summary>
    /// <remarks>
    /// Processes SDP media attributes as documented in RFC 6184 RTP Payload Format for
    /// H.264 Video (https://tools.ietf.org/html/rfc6184).
    /// </remarks>
    [MediaFormatName("H264")]
    internal sealed class H264SinkFactory : H26xSinkFactory, IStreamSinkFactory
    {
        private const string packetizationMode = "packetization-mode=";
        private const string profileLevelId = "profile-level-id=";
        private const int profileLevelIdByteCount = 3;
        private const string spps = "sprop-parameter-sets=";

        //  This code only supports Packetization Mode 0 and 1.
        private readonly int _packetMode = 1;
        private readonly byte[] _profileLevel = null;
        private readonly List<byte[]> _parameterSets = new List<byte[]>();

        /// Initializes a converter with the provided <see cref="RtpMapAttribute"/> source object.
        /// <param name="rtpMap"></param>
        public H264SinkFactory(RtpMapAttribute rtpMap) : base(rtpMap)
        {
            try
            {
                //  Because the member fields the values are stored in are read-only a chained
                //  Where() is not permitted.
                foreach (string item in GetFormatParameters())
                {
                    item.FmtpParser(packetizationMode, ref _packetMode, packetMode => {
                        if (packetMode > 1 || packetMode < 0)
                            throw new ArgumentException("Packetization mode not supported", "rtpMap");
                    });

                    item.FmtpParser(profileLevelId, ref _profileLevel, profileLevelIdByteCount);

                    item.FmtpParser(spps, ref _parameterSets, true);
                }
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException("No associated Fmtp attribute", "rtpMap");
            }
        }

        public void ConnectStreamSink(object container, Session session)
        {
            H264FrameBuffer frameBuffer = new H264FrameBuffer();
            Avc1TrackWriter track = new Avc1TrackWriter(container, _rtpMap.ClockRate,
                _profileLevel, _parameterSets,
                frameBuffer);
            frameBuffer.Connect(track, session);
        }

        public Type GetContainerType()
        {
            return typeof(ContainerWriter);
        }
    }
}
